<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Reservations extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'reservations';
    protected $fillable = [
        'transaction_id',
        'start_time',
        'end_time',
    ];
}
