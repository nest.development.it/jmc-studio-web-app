<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Mail\auth\RegisterVerificationMail;
use App\Mail\auth\ResetPasswordMail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function login(Request $request) {
        if (!Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], true)) {
            return response()->json(['message' => 'Email atau password tidak ditemukan'], 403);
        }

        $user= User::firstWhere('email', $request->input('email'));
        $redirectTo= '';

        if ($user->is_admin == 1) { //login admin
            $redirectTo= 'admin.'.url('/');
        } else { //login customer
            if ($user->is_verified == 0) {
                $isEmailVerificationExpired= DB::table('user_verifications')
                    ->where([
                        ['user_id', '=', $user->id],
                        ['expired_at', '<', Carbon::now()]
                    ])
                    ->first();

                if ($isEmailVerificationExpired) {
                    try {
                        Mail::to($user->email)->send(new RegisterVerificationMail($isEmailVerificationExpired->token, $user));

                        return response()->json(['message' => 'Harap verifikasi email anda terlebih dahulu, silahkan cek email anda'], 200);
                    } catch (\Throwable $th) {
                       return response()->json(['message' => 'Terjadi kesalahan pada server, gagal mengirimkan email untuk verifikasi'], 500);
                    }
                }

                return response()->json(['message' => 'Harap verifikasi email anda terlebih dahulu'], 401);
            }

            $redirectTo= '/';
        }

        return response()->json([
            'message' => 'Masuk berhasil',
            'redirectTo' => $redirectTo
        ]);
    }

    public function register(Request $request) {
        unset($request['confirm_password']);

        $isUserExist= User::firstWhere('email', $request->input('email'));

        if ($isUserExist) {
            return response()->json(['message' => 'Email sudah terdaftar, gunakan email lain'], 409);
        }

        $request['password']= Hash::make($request->input('password'));

        try {
            $createdUser= User::create($request->input());
            $token= md5(uniqid($createdUser->email, true));
            $createdUserVerification= DB::table('user_verifications')->insert([
                'token' =>  $token,
                'user_id' => $createdUser->id,
                'expired_at' => Carbon::tomorrow()->format('Y-m-d H:i:s')
            ]);

            if (!$createdUser && !$createdUserVerification) {
                return response()->json(['message' => 'Terjadi kesalahan pada server, silahkan mencoba lagi'], 500);
            }

            try {
                Mail::to($createdUser->email)->send(new RegisterVerificationMail($token, $createdUser));
            } catch (\Throwable $th) {
               return response()->json(['message' => 'Terjadi kesalahan pada server, gagal mengirimkan email untuk verifikasi'], 500);
            }

        } catch (\Throwable $th) {
            return response()->json(['message' => 'Terjadi kesalahan pada server, silahkan mencoba lagi'], 500);
        }

        return response()->json(['message' => 'Registrasi akun baru berhasil, silahkan cek email untuk verifikasi']);
    }

    public function registerVerification($token) {
        $isTokenExist= DB::table('user_verifications')
            ->where([
                ['token', '=', $token],
                ['expired_at', '>=', Carbon::now()]
            ])
            ->first();

        if (!$isTokenExist) {
            abort(404);
        }

        $user= User::find($isTokenExist->user_id);

        if (!$user || $user->is_verified != 0) {
            abort(404);
        }

        $user->is_verified= 1;

        if (!$user->save()) {
            abort(404);
        }

        $removeVerificationToken= DB::table('user_verifications')->where('user_id', $user->id)->delete();

        if (!$removeVerificationToken) {
            abort(404);
        }

        echo "<script>alert('Berhasil melakukan verifikasi email!')</script>";

        return redirect('/');
    }

    public function requestResetPassword(Request $request) {
        $isEmailExist= User::firstWhere('email', $request->input('email'));

        if (!$isEmailExist) {
            return response()->json(['message' => 'Email tidak ditemukan, permintaan ubah password gagal'], 404);
        }

        try {
            $token= md5(uniqid($isEmailExist->email, true));
            $createResetPassword= DB::table('user_verifications')->insert([
                'token' => $token,
                'user_id' => $isEmailExist->id,
                'expired_at' => Carbon::tomorrow()->format('Y-m-d H:i:s')
            ]);

            if (!$createResetPassword) {
                return response()->json(['message' => 'Terjadi kesalahan pada server, silahkan mencoba lagi'], 500);
            }

            try {
                Mail::to($isEmailExist->email)->send(new ResetPasswordMail($token, $isEmailExist));
            } catch (\Throwable $th) {
                return response()->json(['message' => 'Terjadi kesalahan pada server, gagal mengirimkan email untuk ubah password'], 500);
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => 'Terjadi kesalahan pada server, silahkan mencoba lagi'], 500);
        }

        return response()->json(['message' => 'Permintaan ubah password berhasil, silahkan cek email anda']);
    }

    public function resetPassword(Request $request) {
        $isTokenExist= DB::table('user_verifications')
            ->where([
                ['token', '=', $request->input('token')],
                ['expired_at', '>=', Carbon::now()]
            ])
            ->first();

        if (!$isTokenExist) {
            return response()->json(['message' => 'Reset password token tidak sesuai, silahkan coba lagi'], 404);
        }

        $isUserExist= User::find($isTokenExist->user_id);

        if (!$isUserExist) {
            return response()->json(['message' => 'User tidak ditemukan, silahkan coba lagi'], 404);
        }

        $isUserExist->password= Hash::make($request->input('password'));

        if (!$isUserExist->save()) {
            return response()->json(['message' => 'Terjadi kendala pada server, silahkan coba lagi'], 500);
        }

        $removeResetPasswordToken= DB::table('user_verifications')->where('user_id', $isUserExist->id)->delete();

        if (!$removeResetPasswordToken) {
            abort(404);
        }

        return response()->json(['message' => 'Ubah password berasil']);
    }

    public function logout() {
        Auth::logout();

        return redirect('/');
    }
}
