<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ViewController extends Controller
{
    public function requestResetPassword() {
        return view('auth.pages.request-reset-password');
    }

    public function resetPassword($token) {
        $isTokenExist= DB::table('user_verifications')
        ->where([
            ['token', '=', $token],
            ['expired_at', '>=', Carbon::now()]
        ])
        ->first();

        if (!$isTokenExist) {
            abort(404);
        }

        return view('auth.pages.reset-password', ['token' => $token]);
    }
}
