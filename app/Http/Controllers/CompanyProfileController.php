<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CompanyProfileController extends Controller
{
    public function landing() {
        return view('company-profile.landing');
    }

    public function services() {
        return view('company-profile.services');
    }

    public function aboutUs() {
        return view('company-profile.about-us');
    }
}
