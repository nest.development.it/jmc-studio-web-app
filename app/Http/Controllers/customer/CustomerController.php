<?php

namespace App\Http\Controllers\customer;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    public function updateAccount(Request $request) {
        try {
            $updateUser= User::where('id', Auth::user()->id)->update($request->input());

            if (!$updateUser) {
                return response()->json(['message' => 'Terjadi kesalahan pada server, silahkan mencoba lagi'], 500);
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => 'Terjadi kesalahan pada server, silahkan mencoba lagi'], 500);
        }

        return response()->json(['message' => 'Perubahan tersimpan.']);
    }

    public function changePassword(Request $request) {
        if (!Hash::check($request->input('old_password'), Auth::user()->password)) {
            return response()->json(['message' => 'Password lama salah'], 403);
        }

        $updateUserPassword= User::where(['id' => Auth::user()->id])
            ->update(['password' => Hash::make($request->input('new_password'))]);

        if (!$updateUserPassword) {
            return response()->json(['Terjadi kesalahan pada server, silahkan mencoba lagi'], 500);
        }

        return response()->json(['message' => 'Ubah password berhasil']);
    }
}
