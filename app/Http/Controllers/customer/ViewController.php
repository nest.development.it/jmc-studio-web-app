<?php

namespace App\Http\Controllers\customer;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Reservations;
use App\Models\Transaction;
use App\Models\Holiday;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

use function PHPUnit\Framework\isNull;

class ViewController extends Controller
{
    public function account(Request $request) {
        $menu= 'account-information';

        if ($request->has('menu') && $request->get('menu')) {
            $menu= $request->get('menu');
        }

        return view('customer.pages.account.index', ['menu' => $menu]);
    }

    public function reservation(Request $request){
        if(Auth::user()){
            $data['product'] = Product::get();
            $data['reservation'] = Reservations::get();
            return view('customer.pages.reservation.index', $data);
        }else{
            Alert::alert('Anda belum login!', '', 'success');
            return back();
        }
    }

    public function getHistory(Request $req){
        if(Auth::user()){
            $data = Transaction::select('id','product_id','total_price','status','created_at','payment_timestamp')->where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get();
            return Datatables::of($data)
                ->addColumn('detail',function($row){
                    $detailBtn = '<button id="customer-transaction-detail" type="button" value="'.$row->id.'" class="btn_yellow">Detail</button>';

                    return $detailBtn;
                })
                ->addColumn('priceFormat',function($row){
                    return "Rp " . number_format($row->total_price,2,',','.');
                })
                ->addColumn('productName',function($row){
                    return Product::find($row->product_id)->name;
                })
                ->addColumn('status_btn', function($row){
                    $statusBtn = null;
                    if($row->status == 0 && $row->payment_timestamp === null){
                        $statusBtn = '<button id="customer-transaction-payment" type="button" class="btn_red" value="'.$row->id.'">Bayar</button></form>';
                    }else if($row->status == 1){
                        $statusBtn = '<button type="button" class="btn_green_lunas" disable>Lunas</button>';
                    }else{
                        $statusBtn = '<button type="button" class="btn_yellow" disable>Pending</button>';
                    }

                    return $statusBtn;
                })
                ->escapeColumns([])
                ->make(true);

            return response()->json($data,200);
        }
    }

    public function getCalendarEvents(Request $req){
        $data['product'] = Product::get();
        $data['transaction'] = Transaction::get();
        $data['reservation'] = Reservations::get();
        $data['holidays'] = Holiday::get();
        $data['message'] = "Pilih tanggal reservasi studio.";

        return response()->json($data,200);
    }
}
