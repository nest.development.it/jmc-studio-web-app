<?php

namespace App\Http\Controllers\customer;

use App\Models\User;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\Reservations;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Carbon;
use Yajra\DataTables\Facades\DataTables;

class TransactionController extends Controller
{
    public function newReservation(Request $req){
        $validated = $req->validate([
            'product' => 'required',
            'start_time' => 'required',
            'end_time' => 'required'
        ]);

        //If Validated
        $product = explode('$',strval($req->input('product')));
        $productId = $product[0];
        $productPrice = $product[1];
        $userId = Auth::user()->id;
        $start_time = $req->input('start_time');
        $end_time = $req->input('end_time');

        $transaction = Transaction::create([
            'product_id' => $productId,
            'user_id' => $userId,
            'payment_timestamp' => null,
            'total_price' => $productPrice,
            'status' => 0,
        ]);

        if($productId == 1){
            //Pertemuan 1
            Reservations::create([
                'transaction_id' => $transaction->id,
                'start_time' => $start_time,
                'end_time' => $end_time,
            ]);

            //Pertemuan 2
            if($req->has('courseReservation2')){
                $req->validate([
                    'start_time_course2' => 'required',
                    'end_time_course2' => 'required',
                ]);
                Reservations::create([
                    'transaction_id' => $transaction->id,
                    'start_time' => $req->input('start_time_course2'),
                    'end_time' => $req->input('end_time_course2'),
                ]);
            }

            //Pertemuan 3
            if($req->has('courseReservation3')){
                $req->validate([
                    'start_time_course3' => 'required',
                    'end_time_course3' => 'required',
                ]);
                Reservations::create([
                    'transaction_id' => $transaction->id,
                    'start_time' => $req->input('start_time_course3'),
                    'end_time' => $req->input('end_time_course3'),
                ]);
            }

            //Pertemuan 4
            if($req->has('courseReservation4')){
                $req->validate([
                    'start_time_course4' => 'required',
                    'end_time_course4' => 'required',
                ]);
                Reservations::create([
                    'transaction_id' => $transaction->id,
                    'start_time' => $req->input('start_time_course4'),
                    'end_time' => $req->input('end_time_course4'),
                ]);
            }
        }else if($productId == 2){
            //Live Stream
            Reservations::create([
                'transaction_id' => $transaction->id,
                'start_time' => $start_time,
                'end_time' => $end_time,
            ]);

            //Check Sound
            if($req->has('streamingChecksound')){
                $req->validate([
                    'start_time_streaming' => 'required',
                    'end_time_streaming' => 'required',
                ]);
                Reservations::create([
                    'transaction_id' => $transaction->id,
                    'start_time' => $req->input('start_time_streaming'),
                    'end_time' => $req->input('end_time_streaming'),
                ]);
            }
        }else{
            Reservations::create([
                'transaction_id' => $transaction->id,
                'start_time' => $start_time,
                'end_time' => $end_time,
            ]);
        }

        return response()->json(['message' => 'berhasil menyimpan data reservasi.'], 200);
    }

    public function getTransaction(Request $req){
        $data['product'] = Product::get();
        $data['transaction'] = Transaction::get();
        $data['reservation'] = Reservations::get();
        $data['message'] = "Pilih tanggal reservasi studio.";

        return response()->json($data,200);
    }

    public function uploadPayment(Request $req){
        $validated = $req->validate([
            'bukti_transfer' => 'required|image|max:25000',
        ]);

        if ($req->hasFile('bukti_transfer')) {

            $transactionId = $req->input('transactionId');
            $file = $req->file('bukti_transfer');
            $filename = "payment-".$transactionId.".png";
            $path = Storage::putFileAs('images/payment',$file,$filename);

            //Set status to pending
            $currTrans = Transaction::find($transactionId);
            $currTrans->payment_timestamp = Carbon::now()->format('Y-m-d H:i:s');
            $currTrans->save();
        }

        $data['message'] = "Bukti transfer sudah disimpan.";
        return response()->json($data, 200);
    }

    public function getDetail(Request $req, $id){
        $data['transaction'] = Transaction::find($id);
        $data['product'] = Product::find($data['transaction']->product_id);
        $data['reservation'] = Reservations::where('transaction_id','=',$data['transaction']->id)->get();
        $data['totalHarga'] = "Rp " . number_format($data['transaction']->total_price,2,',','.');

        return response()->json($data,200);
    }
}
