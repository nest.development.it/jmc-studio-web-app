<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ProductController extends Controller
{
    //
    public function save_product(Request $req){
        $req->validate([
            'name' => 'required',
            'description' => 'required',
            'duration' => 'required',
            'price' => 'required'
        ]);

        Product::create([
            'name' => $req->input('name'),
            'description' => $req->input('description'),
            'duration' => $req->input('duration'),
            'price' => $req->input('price'),
            'cut_off' => 0
        ]);

        return response()->json(['message' => "Berhasil menyimpan produk baru."], 200);
    }

    public function show(Request $request,$id)
    {
        $product = Product::find($id);
        $param["data_product"] = $product;
        return view('admin.pages.detail_product',$param);
    }

    public function edit(Request $request, $id){

        // return response()->json($request);
        $product = Product::find($id);
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->duration = $request->input('duration');
        $product->price = $request->input('price');
        $product->cut_off = $request->input('cut_off');
        $product->save();
        //Alert::alert('Data Updated !', '', 'success');

        return response()->json(['message' => 'Produk sudah diupdate.', 'user' => $product], 200);
    }

    public function destroy(Request $request)
    {
        $product = Product::find($request->input('productId'));

        $transaction = Transaction::where('product_id','=',$product->id)->get();

        if(count($transaction) == 0){
            $product->delete();
            //Alert::alert('Data Deleted !', '', 'success');
            return response()->json(['message' => 'Produk sudah terhapus.', 'user' => $product], 200);
        }else{
            return response()->json(['message' => 'Tidak bisa! Produk ada pada 1 atau lebih transaksi', 'transaction' => $transaction], 400);
        }

    }
}
