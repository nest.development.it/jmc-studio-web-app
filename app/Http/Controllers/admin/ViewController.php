<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Holiday;
use App\Models\Product;
use App\Models\User;
use App\Models\Reservations;
use Illuminate\Http\Request;
use App\Models\Transaction;
use Yajra\DataTables\Facades\DataTables;

class ViewController extends Controller
{
    //List Customer
    public function list_customer()
    {
        return view('admin.pages.list_customer');
    }

    public function get_list_customer(Request $request)
    {
        if ($request->ajax()) {
            $data = User::select('id','name','email')->latest()->get();
            return Datatables::of($data)
                // ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="user/'.$row->id.'" class="btn_yellow">Edit</a>';
                    $btn = '<form id="btn-account-delete">'.$btn.'<input name="accountId" type="hidden" value="'.$row->id.'"><button type="submit" class="btn_red">Delete</button></form>';
                    return $btn;
                })
                ->make(true);
        }
    }

    //List Product
    public function list_product()
    {
        return view('admin.pages.list_product');
    }

    public function get_list_product(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::select('id','name','duration','price')->latest()->get();
            return Datatables::of($data)
                // ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="product/'.$row->id.'" class="btn_yellow">Edit</a>';
                    $btn = '<form id="btn-product-delete">'.$btn.'<input name="productId" type="hidden" value="'.$row->id.'"><button type="submit" class="btn_red">Delete</button></form>';
                    return $btn;
                })
                ->make(true);
        }
    }

    //Create Product
    public function create_product()
    {
        return view('admin.pages.create_product');
    }

    //Holiday Days
    public function holidayDays() {
        return view('admin.pages.holiday-days.list-day');
    }

    public function setHolidayDays() {
        return view('admin.pages.holiday-days.set-day');
    }

    //Transaction
    public function transactionApproval(){
        return view('admin.pages.transaction-approval');
    }

    public function transactionCashier(){
        $data['product'] = Product::get();

        return view('admin.pages.cashier', $data);
    }

    public function transactionReport(){
        return view('admin.pages.list_transaction');
    }

    public function getTransactionApproval(Request $req){
        $data = Transaction::whereNotNull('payment_timestamp')->where('status','=',0)->get();
        return Datatables::of($data)
            ->addColumn('product_name', function($row){
                return Product::find($row->product_id)->name;
            })
            ->addColumn('user_name', function($row){
                if($row->user_id > 0){
                    return User::find($row->user_id)->name;
                }else{
                    return "Cashier Admin";
                }
            })
            ->addColumn('user_email', function($row){
                if($row->user_id > 0){
                    return User::find($row->user_id)->email;
                }else{
                    return "Cashier Admin";
                }
            })
            ->addColumn('user_phone', function($row){
                if($row->user_id > 0){
                    return User::find($row->user_id)->phone_number;
                }else{
                    return "Cashier Admin";
                }
            })
            ->addColumn('price_format', function($row){
                return "Rp " . number_format($row->total_price,2,',','.');;
            })
            ->addColumn('action', function($row){
                $btn = '<button type="button" value="'.$row->id.'|'.$row->payment_timestamp.'" class="payment-image btn_yellow">Bukti Pembayaran</button>';
                $btn = '<form id="admin-transaction-approval"><input name="transactionId" type="hidden" value="'.$row->id.'">'.$btn.'<button type="submit" class="btn_red">Approve</button></form>';
                return $btn;
            })
            ->make(true);

        return response()->json($data,200);
    }

    //Holiday

    public function getRecurringHolidays(Request $req){
        $data = Holiday::where('isRecurrent','=',1)->get();

        return Datatables::of($data)
            ->addColumn('daysOfWeek', function($row){
                $day = "Minggu";

                if($row->recurrent_day == 1){
                    $day = "Senin";
                }else if($row->recurrent_day == 2){
                    $day = "Selasa";
                }else if($row->recurrent_day == 3){
                    $day = "Rabu";
                }else if($row->recurrent_day == 4){
                    $day = "Kamis";
                }else if($row->recurrent_day == 5){
                    $day = "Jumat";
                }else if($row->recurrent_day == 6){
                    $day = "Sabtu";
                }

                return $day;
            })
            ->addColumn('action', function($row){
                $btn = '<a href="holiday/'.$row->id.'" class="btn_yellow">Edit</a>';
                $btn = '<form id="btn-holiday-delete">'.$btn.'<input name="holidayId" type="hidden" value="'.$row->id.'"><button type="submit" class="btn_red">Delete</button></form>';
                return $btn;
            })
            ->make(true);
    }

    public function getNonrecurringHolidays(Request $req){
        $data = Holiday::where('isRecurrent','=',0)->get();

        return Datatables::of($data)
            ->addColumn('action', function($row){
                $btn = '<a href="holiday/'.$row->id.'" class="btn_yellow">Edit</a>';
                $btn = '<form id="btn-holiday-delete">'.$btn.'<input name="holidayId" type="hidden" value="'.$row->id.'"><button type="submit" class="btn_red">Delete</button></form>';
                return $btn;
            })
            ->make(true);
    }

    public function getCalendarEvents(Request $req){
        $data['product'] = Product::get();
        $data['transaction'] = Transaction::get();
        $data['reservation'] = Reservations::get();
        $data['holidays'] = Holiday::get();
        $data['message'] = "Pilih tanggal reservasi studio.";

        return response()->json($data,200);
    }
}
