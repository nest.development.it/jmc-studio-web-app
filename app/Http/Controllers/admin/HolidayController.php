<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Holiday;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class HolidayController extends Controller
{
    //
    public function Create(Request $req,$isRecurrent)
    {
        if($isRecurrent == 0){
            $req->validate([
                'name' => 'required',
                'description' => 'required',
                'start_datetime' => 'required',
                'end_datetime' => 'required',
            ]);

            $holiday = Holiday::create([
                'name' => $req->input('name'),
                'description' => $req->input('description'),
                'start_timestamp' => $req->input('start_datetime'),
                'end_timestamp' => $req->input('end_datetime'),
                'recurrent_day' => null,
                'recurrent_start_time' => null,
                'recurrent_end_time' => null,
                'isRecurrent' => 0,
            ]);

            return response()->json(['message' => "Data berhasil disimpan."],200);

        }else if($isRecurrent == 1){
            $req->validate([
                'name' => 'required',
                'description' => 'required',
                'dayOfWeek' => 'required',
                'start_time' => 'required',
                'end_time' => 'required',
            ]);

            $holiday = Holiday::create([
                'name' => $req->input('name'),
                'description' => $req->input('description'),
                'start_timestamp' => null,
                'end_timestamp' => null,
                'recurrent_day' => $req->input('dayOfWeek'),
                'recurrent_start_time' => $req->input('start_time'),
                'recurrent_end_time' => $req->input('end_time'),
                'isRecurrent' => 1,
            ]);

            return response()->json(['message' => "Data berhasil disimpan."],200);
        }
    }
}
