<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    //
    public function show(Request $request,$id)
    {
        $user = User::find($id);
        $param["data_user"] = $user;
        return view('admin.pages.detail_customer',$param);
    }
    public function edit(Request $request, $id){

        // return response()->json($request);
        $user = User::find($id);
        $user->name = $request->input('name');
        $user->phone_number = $request->input('phone_number');
        $user->date_of_birth = $request->input('date_of_birth');
        $user->save();
        //Alert::alert('Data Updated !', '', 'success');

        return response()->json(['message' => 'Akun sudah terhapus.', 'user' => $user], 200);
    }
    public function destroy(Request $request)
    {
        $user = User::find($request->input('accountId'));
        $user->delete();
        //Alert::alert('Data Deleted !', '', 'success');
        return response()->json(['message' => 'Akun sudah terhapus.', 'user' => $user], 200);
    }
}
