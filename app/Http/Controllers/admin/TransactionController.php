<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use App\Models\Reservations;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Session;

use function PHPUnit\Framework\isEmpty;
use function PHPUnit\Framework\isNull;

class TransactionController extends Controller
{

    //
    public function show(Request $request,$id)
    {
        $user = User::find($id);
        $param["data_user"] = $user;
        return view('admin.pages.detail_customer',$param);
    }

    //Verify Payment
    public function verifyPayment(Request $req){
        $transaction = Transaction::find($req->input('transactionId'));
        $transaction->status = 1;
        $transaction->save();

        return response()->json(['message' => 'Transaksi sudah diapprove.', 'user' => $transaction], 200);
    }

    public function cashierInput(Request $req){
        $validated = $req->validate([
            'product' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'payment' => 'required',
        ]);

        //If Validated
        $product = explode('$',strval($req->input('product')));
        $productId = $product[0];
        $productPrice = $product[1];
        $userId = 0;
        $payment = $req->input('payment');
        $start_time = $req->input('start_time');
        $end_time = $req->input('end_time');

        $transaction = Transaction::create([
            'product_id' => $productId,
            'user_id' => $userId,
            'payment_timestamp' => Carbon::today(),
            'total_price' => $productPrice,
            'status' => 1,
        ]);

        Reservations::create([
            'transaction_id' => $transaction->id,
            'start_time' => $start_time,
            'end_time' => $end_time,
        ]);

        return response()->json(['message' => 'berhasil menyimpan data reservasi.'], 200);

    }

    public function getPaymentImage(Request $req, $id){
        $path = storage_path('app/images/payment/payment-'.$id.'.png');
        $data = file_get_contents($path);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $base64 = base64_encode($data);

        $src = 'data:image/' . $type . ';base64,' . $base64;

        //$data['imageSrc'] =  $src;

        return response()->json($src,200);
    }

    public function getReport(Request $req){
        if(!Session::has('transReport')){
            $transaction = Transaction::get();
            Session::put('transReport',$transaction);
        }

        $data = Session::get('transReport');
        if($data == "None"){
            $data = [];
            return Datatables::of($data)
            ->addColumn('createdDateFormat', function($row){
                return Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->isoFormat('dddd, D MMMM Y');
            })
            ->addColumn('action', function($row){
                $btn = '<button type="button" class="btn-detail-transaction btn_yellow" value="'.$row->id.'">Show</button>';
                return $btn;
            })
            ->make(true);
        }else{
            return Datatables::of($data)
            ->addColumn('createdDateFormat', function($row){
                return Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->isoFormat('dddd, D MMMM Y');
            })
            ->addColumn('action', function($row){
                $btn = '<button type="button" class="admin-detail-transaction btn_yellow" value="'.$row->id.'">Show</button>';
                return $btn;
            })
            ->make(true);
        }
    }

    public function getDetail(Request $req, $id){
        $data['transaction'] = Transaction::find($id);
        $data['product'] = Product::find($data['transaction']->product_id);
        $data['reservation'] = Reservations::where('transaction_id','=',$data['transaction']->id)->get();
        $data['totalHarga'] = "Rp " . number_format($data['transaction']->total_price,2,',','.');

        return response()->json($data,200);
    }

    public function updateReport(Request $req){
        $req->validate([
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $transaction = Transaction::all()->whereBetween('created_at',[$req->input('start_date'),$req->input('end_date')]);

        if(count($transaction) == 0){
            Session::put('transReport',"None");
        }else{
            Session::put('transReport',$transaction);
        }


        return response()->json([
                'message' => 'Berhasil mengubah data',
                'session' => Session::get('transReport'),
            ]
            , 200);
    }
}
