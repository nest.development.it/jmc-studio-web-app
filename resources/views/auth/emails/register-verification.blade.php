<!DOCTYPE html>
<html lang="en">
    <body style="color: black;">
        <header>
            <h1>Verifikasi akun baru</h1>
        </header>
        <main>
            <div style="width: 50%; margin: 0 auto;">
                <div style="text-align: center;">
                    <h3 style="margin-bottom: 0;">Halo {{ $user->name }},</h3>
                    <p>Kami mengirimkan surel ini karena anda baru saja melakukan pendaftaran akun baru. Klik tautan dibawah untuk memverifikasi email anda.</p>
                    <div style="margin-top: 1.5rem; margin-bottom: 1.5rem;">
                        <a href="{{ url('/auth/register-verification/'.$token) }}">
                            <button>Verikasi sekarang</button>
                        </a>
                    </div>
                </div>
                <div style="text-align: center; margin-top: 2rem;">- JMC Studio -</div>
            </div>
        </main>
    </body>
</html>
