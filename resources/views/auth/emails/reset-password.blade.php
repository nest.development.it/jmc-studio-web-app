<!DOCTYPE html>
<html lang="en">
    <body style="color: black;">
        <header>
            <h1>Permintaan ubah kata sandi</h1>
        </header>
        <main>
            <div style="width: 50%; margin: 0 auto;">
                <div style="text-align: center;">
                    <h3 style="margin-bottom: 0;">Halo {{ $user->name }},</h3>
                    <p>Kami mengirimkan surel ini karena anda baru saja melakukan permintaan untuk mengubah kata sandi anda. Klik tautan dibawah untuk mengubah kata sandi anda.</p>
                    <div style="margin-top: 1.5rem; margin-bottom: 1.5rem;">
                        <a href="{{ url('/reset-password/'.$token) }}">
                            <button>Ubah sekarang</button>
                        </a>
                    </div>
                </div>
                <p>Jika anda tidak mengajukan permintaan ubah kata sandi, anda bisa abaikan surel ini. Kata sandi anda tidak akan terubah.</p>
                <div style="text-align: center; margin-top: 2rem;">- JMC Studio -</div>
            </div>
        </main>
    </body>
</html>
