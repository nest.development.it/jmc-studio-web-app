@extends('layouts.auth-layout')

@section('title')
<title>Ubah Password</title>
@endsection

@section('main')
<div class="flex flex-col gap-y-6 p-6">
    <a href="/" class="flex items-center gap-x-3">
        <i class="fas fa-chevron-left"></i>
        <span class="cursor-pointer text-sm font-semibold">Kembali</span>
    </a>
    <form id="request-reset-password-form">
        <div class="flex flex-col gap-y-5 text-left">
            <div class="flex flex-col gap-y-2">
                <label class="text-sm text-black text-opacity-80">Email</label>
                @component('shared.components.input', [
                    'type' => 'email',
                    'name' => 'email',
                    'placeholder' => 'Email anda'
                ])
                @endcomponent
            </div>

            <button type="submit" class="rounded-full bg-primary text-sm text-white w-1/2 mx-auto px-2 py-2 focus:outline-none">Kirim</button>
        </div>
    </form>
    <div class="flex gap-x-1 italic text-red-500">
        <i class="fas fa-asterisk text-tiny"></i>
        <span class="text-xs">Masukkan email anda untuk permintaan ubah password</span>
    </div>
</div>
@endsection
