@extends('layouts.auth-layout')

@section('title')
<title>Ubah Password</title>
@endsection

@section('main')
<div class="flex flex-col gap-y-6 p-6">
    <h2 class="text-lg font-semibold text-center">Ubah Password</h2>
    <form id="reset-password-form">
        <div class="flex flex-col gap-y-5 text-left">
            <div class="flex flex-col gap-y-2">
                <label class="text-sm text-black text-opacity-80">Password</label>
                @component('shared.components.input', [
                    'type' => 'password',
                    'name' => 'password',
                    'placeholder' => 'Password anda'
                ])
                @endcomponent
            </div>
            <div class="flex flex-col gap-y-2">
                <label class="text-sm text-black text-opacity-80">Konfirmasi password</label>
                @component('shared.components.input', [
                    'type' => 'password',
                    'name' => 'confirmation_password',
                    'placeholder' => 'Konfirmasi password anda'
                ])
                @endcomponent
            </div>
            <input type="hidden" name="token" value="{{ $token }}">

            <button type="submit" class="rounded-full bg-primary text-sm text-white w-1/2 mx-auto px-2 py-2 focus:outline-none">Ubah</button>
        </div>
    </form>
</div>
@endsection
