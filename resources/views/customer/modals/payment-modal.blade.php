<div id="customer-payment-modal">
    @component('shared.components.modal')
        @slot('content')
        <div class="flex flex-col gap-y-4 rounded-2xl bg-white w-2/4 mx-auto p-8">
            <button class="modal-close-button ml-auto focus:outline-none">
                <i class="fas fa-times text-primary"></i>
            </button>
            <h3 class="text-lg">Bukti Pembayaran</h3>

            <form action="" enctype="multipart/form-data" id="form-customer-payment">
                <input class="input-transaction-id" type="hidden" name="transactionId" value="">
                <div class="flex flex-col gap-y-2">
                    <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                        Upload Bukti Pembayaran
                        <i class="fas fa-asterisk text-tiny text-red-500"></i>
                    </label>
                    @component('shared.components.input', [
                        'type' => 'file',
                        'name' => 'bukti_transfer',
                        'placeholder' => 'Konfirmasi password'
                    ])
                    @endcomponent
                </div>
                <div class="flex flex-col gap-y-2 py-4">
                    <button type="submit" class="col-span-2 rounded-full bg-primary text-sm text-white w-1/4 mx-auto px-2 py-2 focus:outline-none">Kirim</button>
                </div>
            </form>
        </div>
        @endslot
    @endcomponent
</div>
