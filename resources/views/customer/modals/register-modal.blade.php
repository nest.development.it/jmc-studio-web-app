<div id="register-modal">
    @component('shared.components.modal')
        @slot('content')
        <div class="flex flex-col gap-y-4 rounded-2xl bg-white w-3/4 mx-auto p-8">
            <button class="modal-close-button ml-auto focus:outline-none">
                <i class="fas fa-times text-primary"></i>
            </button>
            <h3 class="text-lg">Daftar</h3>
            <form id="register-form" class="w-full">
                <div class="grid grid-cols-2 gap-5 text-left">
                    <div class="flex flex-col gap-y-2">
                        <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                            Nama Lengkap
                            <i class="fas fa-asterisk text-tiny text-red-500"></i>
                        </label>
                        @component('shared.components.input', [
                            'type' => 'text',
                            'name' => 'name',
                            'placeholder' => 'Nama anda'
                        ])
                        @endcomponent
                    </div>

                    <div class="flex flex-col gap-y-2">
                        <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                            Email
                            <i class="fas fa-asterisk text-tiny text-red-500"></i>
                        </label>
                        @component('shared.components.input', [
                            'type' => 'email',
                            'name' => 'email',
                            'placeholder' => 'Email anda'
                        ])
                        @endcomponent
                    </div>

                    <div class="flex flex-col gap-y-2">
                        <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                            Tanggal Lahir
                            <i class="fas fa-asterisk text-tiny text-red-500"></i>
                        </label>
                        @component('shared.components.input', [
                            'type' => 'date',
                            'name' => 'date_of_birth',
                            'placeholder' => 'Tanggal lahir anda'
                        ])
                        @endcomponent
                    </div>

                    <div class="flex flex-col gap-y-2">
                        <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                            Nomor HP
                            <i class="fas fa-asterisk text-tiny text-red-500"></i>
                        </label>
                        @component('shared.components.input', [
                            'type' => 'text',
                            'name' => 'phone_number',
                            'placeholder' => 'Nomor HP anda'
                        ])
                        @endcomponent
                        <span class="text-xs text-red-500 italic">*) Nomor terdaftar pada Whatsapp</span>
                    </div>

                    <div class="flex flex-col gap-y-2">
                        <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                            Password
                            <i class="fas fa-asterisk text-tiny text-red-500"></i>
                        </label>
                        @component('shared.components.input', [
                            'type' => 'password',
                            'name' => 'password',
                            'placeholder' => 'Password anda'
                        ])
                        @endcomponent
                    </div>

                    <div class="flex flex-col gap-y-2">
                        <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                            Konfirmasi Password
                            <i class="fas fa-asterisk text-tiny text-red-500"></i>
                        </label>
                        @component('shared.components.input', [
                            'type' => 'password',
                            'name' => 'confirm_password',
                            'placeholder' => 'Konfirmasi password'
                        ])
                        @endcomponent
                    </div>

                    <button type="submit" class="col-span-2 rounded-full bg-primary text-sm text-white w-1/4 mx-auto px-2 py-2 focus:outline-none">Daftar</button>
                </div>
            </form>
        </div>
        @endslot
    @endcomponent
</div>
