<div id="fullcalendar-modal">
    @component('shared.components.modal')
        @slot('content')
        <div class="flex flex-col gap-y-4 rounded-2xl bg-white w-full p-8">
            <div class="calendar w-full">

            </div>
        </div>
        @endslot
    @endcomponent
</div>
