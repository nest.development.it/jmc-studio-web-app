<div id="login-modal">
    @component('shared.components.modal')
        @slot('content')
        <div class="flex flex-col gap-y-4 rounded-2xl bg-white w-2/4 mx-auto p-8">
            <button class="modal-close-button ml-auto focus:outline-none">
                <i class="fas fa-times text-primary"></i>
            </button>
            <h3 class="text-lg">Masuk</h3>
            <form id="login-form" class="w-full">
                <div class="flex flex-col gap-y-5 text-left">
                    <div class="flex flex-col gap-y-2">
                        <label class="text-sm text-black text-opacity-80">Email</label>
                        @component('shared.components.input', [
                            'type' => 'email',
                            'name' => 'email',
                            'placeholder' => 'Email anda'
                        ])
                        @endcomponent
                    </div>

                    <div class="flex flex-col gap-y-2">
                        <label class="text-sm text-black text-opacity-80">Password</label>
                        @component('shared.components.input', [
                            'type' => 'password',
                            'name' => 'password',
                            'placeholder' => 'Password anda'
                        ])
                        @endcomponent
                    </div>

                    <button type="submit" class="rounded-full bg-primary text-sm text-white w-1/2 mx-auto px-2 py-2 focus:outline-none">Masuk</button>
                </div>
            </form>
            <span class="text-sm mt-2">
                Lupa <a href="/request-reset-password" class="text-primary font-semibold">password</a> anda?
            </span>
        </div>
        @endslot
    @endcomponent
</div>
