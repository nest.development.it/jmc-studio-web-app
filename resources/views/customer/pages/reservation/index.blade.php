@extends('layouts.base-layout')

@section('title')
<title>Akun Saya</title>
@endsection

@section('main')
<main>
    <section class="relative block h-500-px">
        <div class="absolute top-0 w-full h-full bg-center bg-cover" style="background-image: url('https://images.unsplash.com/photo-1499336315816-097655dcfbda?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=2710&amp;q=80');">
            <span id="blackOverlay" class="w-full h-full absolute opacity-50 bg-black"></span>
        </div>
        <div class="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden h-70-px" style="transform: translateZ(0px)">
            <svg
                class="absolute bottom-0 overflow-hidden"
                xmlns="http://www.w3.org/2000/svg"
                preserveAspectRatio="none"
                version="1.1"
                viewBox="0 0 2560 100"
                x="0"
                y="0">
                <polygon class="text-blueGray-200 fill-current" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </section>

    <section class="relative py-16 bg-blueGray-200">
        <div class="container mx-auto px-20">
            <div class="relative grid grid-cols-3 gap-x-10 -mt-64">
                <div class="rounded-lg shadow-xl col-span-3 bg-white h-fit-content p-14">
                    <h3 class="text-center text-xl text-black text-opacity-75 font-semibold">Reservasi</h3>
                    <form id="customer-reservation-form" class="mt-10">
                        <div class="flex flex-wrap gap-y-5">
                            <div class="flex flex-col gap-y-2 w-full">
                                <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                    Layanan
                                    <i class="fas fa-asterisk text-tiny text-red-500"></i>
                                </label>
                                <select name="product" class="
                                input-product
                                product-dropdown
                                transition delay-70 duration-300 ease-in-out
                                border border-primary border-opacity-25 rounded-lg
                                text-sm
                                focus:border-primary
                                {{ isset($extraClassName) ? $extraClassName : '' }}">
                                    <option disabled selected value> -- Pilih Layanan -- </option>
                                    @foreach ($product as $p)
                                        <option value="{{$p->id}}${{$p->price}}${{$p->duration}}">{{$p->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="flex gap-x-5 w-full">
                                <div class="flex flex-col gap-y-2 w-1/2">
                                    <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                        Tanggal Reservasi
                                        <i class="fas fa-asterisk text-tiny text-red-500"></i>
                                    </label>
                                    @component('shared.components.input', [
                                        'type' => 'datetime-local',
                                        'name' => 'start_time',
                                        'placeholder' => 'Pilih tanggal reservasi...',
                                        'readonly' => '',
                                        'extraClassName' => 'start-date',
                                    ])
                                    @endcomponent
                                </div>
                                <div class="flex flex-col gap-y-2 w-1/2">
                                    <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                        <br>
                                    </label>
                                    @component('shared.components.input', [
                                        'type' => 'datetime-local',
                                        'name' => 'end_time',
                                        'placeholder' => 'Pilih tanggal reservasi...',
                                        'readonly' => '',
                                        'extraClassName' => 'end-date',
                                    ])
                                    @endcomponent
                                </div>

                                <button type="button" class="fullcalendar-controlbtn col-span-2 rounded-full bg-primary text-sm text-white w-1/4 mx-auto px-2 py-2 focus:outline-none">
                                    Pilih Jadwal
                                </button>
                            </div>

                            <div class="course-reservation flex gap-x-5 w-full hidden">
                                <!-- Pertemuan 2 -->
                                <div class="flex flex-col gap-y-2 w-1/2">
                                    <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                        Pertemuan 2
                                        <input type="checkbox" id="" class="course-reservation2" name="courseReservation2" value="">
                                    </label>
                                    @component('shared.components.input', [
                                        'type' => 'datetime-local',
                                        'name' => 'start_time_course2',
                                        'placeholder' => 'Pilih tanggal reservasi...',
                                        'readonly' => 'true',
                                        'extraClassName' => 'start-date-course2',
                                    ])
                                    @endcomponent
                                </div>
                                <div class="flex flex-col gap-y-2 w-1/2">
                                    <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                        <br>
                                    </label>
                                    @component('shared.components.input', [
                                        'type' => 'datetime-local',
                                        'name' => 'end_time_course2',
                                        'placeholder' => 'Pilih tanggal reservasi...',
                                        'readonly' => 'true',
                                        'extraClassName' => 'end-date-course2',
                                    ])
                                    @endcomponent
                                </div>

                                <button disabled type="button" class="fullcalendar-controlbtn-course2 col-span-2 rounded-full bg-primary text-sm text-white w-1/4 mx-auto px-2 py-2 focus:outline-none">
                                    Pilih Jadwal Pertemuan 2
                                </button>
                            </div>

                            <div class="course-reservation flex gap-x-5 w-full hidden">
                                <!-- Pertemuan 3 -->
                                <div class="flex flex-col gap-y-2 w-1/2">
                                    <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                        Pertemuan 3
                                        <input type="checkbox" id="" class="course-reservation3" name="courseReservation3" value="">
                                    </label>
                                    @component('shared.components.input', [
                                        'type' => 'datetime-local',
                                        'name' => 'start_time_course3',
                                        'placeholder' => 'Pilih tanggal reservasi...',
                                        'readonly' => 'true',
                                        'extraClassName' => 'start-date-course3',
                                    ])
                                    @endcomponent
                                </div>
                                <div class="flex flex-col gap-y-2 w-1/2">
                                    <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                        <br>
                                    </label>
                                    @component('shared.components.input', [
                                        'type' => 'datetime-local',
                                        'name' => 'end_time_course3',
                                        'placeholder' => 'Pilih tanggal reservasi...',
                                        'readonly' => 'true',
                                        'extraClassName' => 'end-date-course3',
                                    ])
                                    @endcomponent
                                </div>

                                <button disabled type="button" class="fullcalendar-controlbtn-course3 col-span-2 rounded-full bg-primary text-sm text-white w-1/4 mx-auto px-2 py-2 focus:outline-none">
                                    Pilih Jadwal Pertemuan 3
                                </button>
                            </div>

                            <div class="course-reservation flex gap-x-5 w-full hidden">
                                <!-- Pertemuan 4 -->
                                <div class="flex flex-col gap-y-2 w-1/2">
                                    <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                        Pertemuan 4
                                        <input type="checkbox" id="" class="course-reservation4" name="courseReservation4" value="">
                                    </label>
                                    @component('shared.components.input', [
                                        'type' => 'datetime-local',
                                        'name' => 'start_time_course4',
                                        'placeholder' => 'Pilih tanggal reservasi...',
                                        'readonly' => 'true',
                                        'extraClassName' => 'start-date-course4',
                                    ])
                                    @endcomponent
                                </div>
                                <div class="flex flex-col gap-y-2 w-1/2">
                                    <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                        <br>
                                    </label>
                                    @component('shared.components.input', [
                                        'type' => 'datetime-local',
                                        'name' => 'end_time_course4',
                                        'placeholder' => 'Pilih tanggal reservasi...',
                                        'readonly' => 'true',
                                        'extraClassName' => 'end-date-course4',
                                    ])
                                    @endcomponent
                                </div>

                                <button disabled type="button" class="fullcalendar-controlbtn-course4 col-span-2 rounded-full bg-primary text-sm text-white w-1/4 mx-auto px-2 py-2 focus:outline-none">
                                    Pilih Jadwal Pertemuan 4
                                </button>
                            </div>

                            <div class="streaming-reservation flex gap-x-5 w-full hidden">
                                <!-- Check Sound -->
                                <div class="flex flex-col gap-y-2 w-1/2">
                                    <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                        Free Check Sound
                                        <input type="checkbox" id="" class="streaming-checksound" name="streamingChecksound" value="">
                                    </label>
                                    @component('shared.components.input', [
                                        'type' => 'datetime-local',
                                        'name' => 'start_time_streaming',
                                        'placeholder' => 'Pilih tanggal reservasi...',
                                        'readonly' => 'true',
                                        'extraClassName' => 'start-date-streaming',
                                    ])
                                    @endcomponent
                                </div>
                                <div class="flex flex-col gap-y-2 w-1/2">
                                    <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                        <br>
                                    </label>
                                    @component('shared.components.input', [
                                        'type' => 'datetime-local',
                                        'name' => 'end_time_streaming',
                                        'placeholder' => 'Pilih tanggal reservasi...',
                                        'readonly' => 'true',
                                        'extraClassName' => 'end-date-streaming',
                                    ])
                                    @endcomponent
                                </div>

                                <button disabled type="button" class="fullcalendar-controlbtn-streaming col-span-2 rounded-full bg-primary text-sm text-white w-1/4 mx-auto px-2 py-2 focus:outline-none">
                                    Pilih Jadwal Check Sound
                                </button>
                            </div>

                            <div class="flex gap-x-5 w-full">
                                <div class="flex flex-col gap-y-2 w-full">
                                    <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                        Total Harga
                                        <i class="fas fa-asterisk text-tiny text-red-500"></i>
                                    </label>
                                    @component('shared.components.input', [
                                        'type' => 'number',
                                        'name' => 'total_price',
                                        'placeholder' => 'Pilih layanan...',
                                        'readonly' => '',
                                        'extraClassName' => 'product-price',
                                    ])
                                    @endcomponent
                                </div>
                            </div>

                            <button type="submit" class="col-span-2 rounded-full bg-primary text-sm text-white w-1/4 mx-auto px-2 py-2 focus:outline-none">
                                Pesan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>



            <!--<div class="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg -mt-64">
                <div class="px-6">
                    <div class="flex flex-wrap justify-center">
                        <div class="w-full lg:w-3/12 px-4 lg:order-2 flex justify-center">
                            <div class="relative">
                                <img
                                    alt="..."
                                    src="{{ asset('assets/notus-admin/team-2-800x800.jpg') }}"
                                    class="shadow-xl rounded-full h-auto align-middle border-none absolute -m-16 -ml-20 lg:-ml-16 max-w-150-px"/>
                            </div>
                        </div>
                        <div class="w-full lg:w-4/12 px-4 lg:order-3 lg:text-right lg:self-center">
                            <div class="py-6 px-3 mt-32 sm:mt-0">
                                <button class="bg-pink-500 active:bg-pink-600 uppercase text-white font-bold hover:shadow-md shadow text-xs px-4 py-2 rounded outline-none focus:outline-none sm:mr-2 mb-1 ease-linear transition-all duration-150" type="button">
                                    Connect
                                </button>
                            </div>
                        </div>
                        <div class="w-full lg:w-4/12 px-4 lg:order-1">
                            <div class="flex justify-center py-4 lg:pt-4 pt-8">
                                <div class="mr-4 p-3 text-center">
                                    <span class="text-xl font-bold block uppercase tracking-wide text-blueGray-600">22</span>
                                    <span class="text-sm text-blueGray-400">Friends</span>
                                </div>
                                <div class="mr-4 p-3 text-center">
                                    <span class="text-xl font-bold block uppercase tracking-wide text-blueGray-600">10</span>
                                    <span class="text-sm text-blueGray-400">Photos</span>
                                </div>
                                <div class="lg:mr-4 p-3 text-center">
                                    <span class="text-xl font-bold block uppercase tracking-wide text-blueGray-600">89</span>
                                    <span class="text-sm text-blueGray-400">Comments</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-12">
                        <h3 class="text-4xl font-semibold leading-normal mb-2 text-blueGray-700 mb-2">Jenna Stones</h3>
                        <div class="text-sm leading-normal mt-0 mb-2 text-blueGray-400 font-bold uppercase">
                            <i class="fas fa-map-marker-alt mr-2 text-lg text-blueGray-400"></i>
                            Los Angeles, California
                        </div>
                        <div class="mb-2 text-blueGray-600 mt-10">
                            <i class="fas fa-briefcase mr-2 text-lg text-blueGray-400"></i>
                            Solution Manager - Creative Tim Officer
                        </div>
                        <div class="mb-2 text-blueGray-600">
                            <i class="fas fa-university mr-2 text-lg text-blueGray-400"></i>
                            University of Computer Science
                        </div>
                    </div>
                    <div class="mt-10 py-10 border-t border-blueGray-200 text-center">
                        <div class="flex flex-wrap justify-center">
                            <div class="w-full lg:w-9/12 px-4">
                                <p class="mb-4 text-lg leading-relaxed text-blueGray-700">
                                    An artist of considerable range, Jenna the name taken by
                                    Melbourne-raised, Brooklyn-based Nick Murphy writes,
                                    performs and records all of his own music, giving it a
                                    warm, intimate feel with a solid groove structure. An
                                    artist of considerable range.
                                </p>
                                <a href="#pablo" class="font-normal text-pink-500">Show more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--.
        </div>
    </section>
</main>
@endsection

@include('customer.modals.fullcalendar-modal')
