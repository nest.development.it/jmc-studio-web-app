<h3 class="text-center text-xl text-black text-opacity-75 font-semibold">Informasi Akun</h3>
<form id="update-account-form" class="mt-10">
    <div class="grid grid-cols-2 gap-5 text-left">
        <div class="flex flex-col gap-y-2">
            <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                Nama Lengkap
                <i class="fas fa-asterisk text-tiny text-red-500"></i>
            </label>
            @component('shared.components.input', [
                'type' => 'text',
                'name' => 'name',
                'value' => Auth::user()->name,
                'placeholder' => 'Nama anda'
            ])
            @endcomponent
        </div>

        <div class="flex flex-col gap-y-2">
            <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                Email
                <i class="fas fa-asterisk text-tiny text-red-500"></i>
            </label>
            @component('shared.components.input', [
                'type' => 'email',
                'name' => 'email',
                'value' => Auth::user()->email,
                'placeholder' => 'Email anda'
            ])
            @endcomponent
        </div>

        <div class="flex flex-col gap-y-2">
            <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                Tanggal Lahir
                <i class="fas fa-asterisk text-tiny text-red-500"></i>
            </label>
            @component('shared.components.input', [
                'type' => 'date',
                'name' => 'date_of_birth',
                'value' => Auth::user()->date_of_birth,
                'placeholder' => 'Tanggal lahir anda'
            ])
            @endcomponent
        </div>

        <div class="flex flex-col gap-y-2">
            <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                Nomor HP
                <i class="fas fa-asterisk text-tiny text-red-500"></i>
            </label>
            @component('shared.components.input', [
                'type' => 'text',
                'name' => 'phone_number',
                'value' => Auth::user()->phone_number,
                'placeholder' => 'Nomor HP anda'
            ])
            @endcomponent
            <span class="text-xs text-red-500 italic">*) Nomor terdaftar pada Whatsapp</span>
        </div>
        <button type="submit" class="col-span-2 rounded-full bg-primary text-sm text-white w-1/4 mx-auto px-2 py-2 focus:outline-none">Simpan</button>
    </div>
</form>
