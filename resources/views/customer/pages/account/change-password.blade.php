<h3 class="text-center text-xl text-black text-opacity-75 font-semibold">Ganti Password</h3>
<form id="change-password-form" class="mt-10">
    <div class="flex flex-wrap gap-y-5">
        <div class="flex flex-col gap-y-2 w-1/2">
            <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                Password lama
                <i class="fas fa-asterisk text-tiny text-red-500"></i>
            </label>
            @component('shared.components.input', [
                'type' => 'password',
                'name' => 'old_password',
                'placeholder' => 'Password lama anda'
            ])
            @endcomponent
        </div>

        <div class="flex gap-x-5 w-full">
            <div class="flex flex-col gap-y-2 w-full">
                <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                    Password baru
                    <i class="fas fa-asterisk text-tiny text-red-500"></i>
                </label>
                @component('shared.components.input', [
                    'type' => 'password',
                    'name' => 'new_password',
                    'placeholder' => 'Password baru anda'
                ])
                @endcomponent
            </div>
            <div class="flex flex-col gap-y-2 w-full">
                <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                    Konfirmasi password baru
                    <i class="fas fa-asterisk text-tiny text-red-500"></i>
                </label>
                @component('shared.components.input', [
                    'type' => 'password',
                    'name' => 'confirm_new_password',
                    'placeholder' => 'Password baru anda'
                ])
                @endcomponent
            </div>
        </div>

        <button type="submit" class="col-span-2 rounded-full bg-primary text-sm text-white w-1/4 mx-auto px-2 py-2 focus:outline-none">
            Simpan
        </button>
    </div>
</form>
