<nav class="flex justify-between gap-x-10 top-0 absolute z-50 w-full px-28 py-2">
    <a href="/">
        <img src="{{ asset('/assets/logos/jmc-studio.png') }}" alt="" class="object-contain w-20">
    </a>
    <ul class="inline-flex items-center gap-x-10 text-white text-sm text-opacity-80">
        <li class="transition delay-100 duration-300 ease-in-out">
            <a href="/">Beranda</a>
        </li>
        <li class="transition delay-100 duration-300 ease-in-out">
            <a href="/services">Layanan</a>
        </li>
        <li class="transition delay-100 duration-300 ease-in-out">
            <a href="/about-us">Tentang Kami</a>
        </li>
    </ul>
    <div class="inline-flex items-center gap-x-5 text-white">
        @guest
        <button id="login-button" class="text-sm rounded-full bg-primary w-fit-content h-fit-content px-3 py-2 hover:bg-opacity-75 focus:outline-none">Masuk</button>
        <button id="register-button" class="text-sm rounded-full bg-primary w-fit-content h-fit-content px-3 py-2 hover:bg-opacity-75 focus:outline-none">Daftar</button>
        @endguest

        @auth
        <div class="inline-flex items-center gap-x-2">
            <i class="fas fa-user-circle text-2xl"></i>
            <a href="/account">
                <span class="text-sm cursor-pointer border-b border-transparent hover:text-opacity-80 hover:border-white">{{ Auth::user()->name }}</span>
            </a>
        </div>
        <a href="/auth/logout">
            <i class="fas fa-sign-out-alt"></i>
        </a>
        @endauth
    </div>
</nav>

@include('customer.modals.login-modal')
@include('customer.modals.register-modal')

