<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#000000" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="shortcut icon" href="{{ asset('/assets/notus-admin/favicon.ico') }}" />
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/assets/notus-admin/apple-icon.png') }}" />

        <!-- styles -->
        @include('shared.styles')
        @yield('extra-css')

        @yield('title')
    </head>
    <body class="text-blueGray-700 antialiased">
        <main>
            <section class="relative w-full h-full py-40 min-h-screen">
                <div class="absolute top-0 w-full h-full bg-blueGray-800 bg-full bg-no-repeat" style="background-image: url({{ asset('/assets/notus-admin/register_bg_2.png') }})"></div>
                <div class="container mx-auto px-4 h-full">
                    <div class="flex content-center items-center justify-center h-full">
                        <div class="w-full lg:w-4/12 px-4">
                            <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-200 border-0">
                                @yield('main')
                            </div>
                        </div>
                    </div>
                </div>

                <footer class="absolute w-full bottom-0 bg-blueGray-800 pb-6">
                    <div class="container mx-auto px-4">
                        <hr class="mb-6 border-b-1 border-blueGray-600" />
                        <div class="flex flex-wrap items-center md:justify-between justify-center">
                            <div class="w-full md:w-4/12 px-4">
                                <div class="text-sm text-white font-semibold py-1 text-center md:text-left">
                                    Copyright © <span id="get-current-year"></span>
                                    <a href="https://www.creative-tim.com?ref=njs-login" class="text-white hover:text-blueGray-300 text-sm font-semibold py-1">
                                        NEST Team
                                    </a>
                                </div>
                            </div>
                            <div class="w-full md:w-8/12 px-4">
                                <ul class="flex flex-wrap list-none md:justify-end justify-center">
                                    <li>
                                        <a href="https://www.creative-tim.com?ref=njs-login" class="text-white hover:text-blueGray-300 text-sm font-semibold block py-1 px-3">
                                            NEST Team
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.creative-tim.com/presentation?ref=njs-login" class="text-white hover:text-blueGray-300 text-sm font-semibold block py-1 px-3">
                                            About Us
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://blog.creative-tim.com?ref=njs-login" class="text-white hover:text-blueGray-300 text-sm font-semibold block py-1 px-3">
                                            Blog
                                        </a
                                        >
                                    </li>
                                    <li>
                                        <a href="https://github.com/creativetimofficial/notus-js/blob/main/LICENSE.md?ref=njs-login" class="text-white hover:text-blueGray-300 text-sm font-semibold block py-1 px-3">
                                            MIT License
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </section>
        </main>

        <!--  scripts -->
        @yield('extra-js')
        @include('shared.auth-scripts')
    </body>
</html>
