<div id="payment-modal">
    @component('shared.components.modal')
        @slot('content')
        <div class="flex flex-col gap-y-4 rounded-2xl bg-white w-fit-content p-8 mx-auto">
            <div class="paymentImage flex flex-col gap-y-2">
                <h1 class="font-bold">Waktu Pembayaran</h1>
                <h1 class="payment-timestamp"></h1>
                <hr>
                <img class="image-container object-contain w-96 mx-auto" src="" alt="">
            </div>
        </div>
        @endslot
    @endcomponent
</div>
