<div id="admin-transaction-modal">
    @component('shared.components.modal')
        @slot('content')
        <div class="flex flex-col gap-y-4 rounded-2xl bg-white w-2/4 mx-auto p-8">
            <button class="modal-close-button ml-auto focus:outline-none">
                <i class="fas fa-times text-primary"></i>
            </button>
            <h3 class="text-lg">Transaction Detail</h3>

            <hr>

            <div class="paymentImage flex flex-col gap-y-2">
                <h1 class="font-bold">Nama Produk : <span class="font-normal namaProduk" id=""></span></h1>
                <h1 class="font-bold">Durasi : <span class="font-normal durasi" id=""></span></h1>
                <h1 class="font-bold">Tanggal : <span class="font-normal tanggal" id=""></span></h1>
                <h1 class="font-bold">Waktu Reservasi : <span class="font-normal waktu" id=""></span></h1>
                <h1 class="font-bold">Total : <span class="font-normal total" id=""></span></h1>
            </div>
        </div>
        @endslot
    @endcomponent
</div>
