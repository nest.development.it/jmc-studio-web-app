@extends('layouts.admin-layout')

@section('header')
    @include('admin.components.header_no_login')
@endsection

@section('content')
    @include('admin.components.coba_content')
@endsection
