@extends('layouts.admin-layout')

@section('title')
JMC | Cashier
@endsection

@section('header')
@include('admin.components.header_no_login')
@endsection

@section('content')
<div class="w-full mb-12 px-4">
    <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded bg-white">
        <div class="rounded-t mb-0 px-4 py-3 border-0">
            <div class="flex flex-wrap items-center">
                <div class="relative w-full px-4 max-w-full flex-grow flex-1">
                    <h3 class="font-semibold text-lg text-blueGray-700">
                        Cashier
                    </h3>
                </div>
            </div>
        </div>
        <div class="flex flex-nowrap p-8 ">
            <form id="admin-cashier-form" class="mt-10">
                @csrf
                <div class="flex flex-wrap gap-y-5">
                    <div class="flex flex-col gap-y-2 w-full">
                        <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                            Layanan
                            <i class="fas fa-asterisk text-tiny text-red-500"></i>
                        </label>
                        <select name="product" class="
                        product-dropdown
                        transition delay-70 duration-300 ease-in-out
                        border border-primary border-opacity-25 rounded-lg
                        text-sm
                        focus:border-primary
                        {{ isset($extraClassName) ? $extraClassName : '' }}">
                            <option disabled selected value> -- Pilih Layanan -- </option>
                            @foreach ($product as $p)
                                <option value="{{$p->id}}${{$p->price}}${{$p->duration}}">{{$p->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="flex gap-x-5 w-full">
                        <div class="flex flex-col gap-y-2 w-1/2">
                            <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                Tanggal Reservasi
                                <i class="fas fa-asterisk text-tiny text-red-500"></i>
                            </label>
                            @component('shared.components.input', [
                                'type' => 'datetime-local',
                                'name' => 'start_time',
                                'placeholder' => 'Pilih tanggal reservasi...',
                                'readonly' => '',
                                'extraClassName' => 'start-date',
                            ])
                            @endcomponent
                        </div>
                        <div class="flex flex-col gap-y-2 w-1/2">
                            <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                <br>
                            </label>
                            @component('shared.components.input', [
                                'type' => 'datetime-local',
                                'name' => 'end_time',
                                'placeholder' => 'Pilih tanggal reservasi...',
                                'readonly' => '',
                                'extraClassName' => 'end-date',
                            ])
                            @endcomponent
                        </div>

                        <button type="button" class="fullcalendar-controlbtn col-span-2 rounded-full bg-primary text-sm text-white w-1/4 mx-auto px-2 py-2 focus:outline-none">
                            Pilih Jadwal
                        </button>
                    </div>

                    <div class="flex gap-x-5 w-full">
                        <div class="flex flex-col gap-y-2 w-full">
                            <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                Total Harga
                                <i class="fas fa-asterisk text-tiny text-red-500"></i>
                            </label>
                            @component('shared.components.input', [
                                'type' => 'number',
                                'name' => 'total_price',
                                'placeholder' => 'Pilih layanan...',
                                'readonly' => '',
                                'extraClassName' => 'product-price',
                            ])
                            @endcomponent
                        </div>
                    </div>

                    <div class="flex gap-x-5 w-full">
                        <div class="flex flex-col gap-y-2 w-full">
                            <label class="inline-flex gap-x-1 text-sm text-black text-opacity-80">
                                Pembayaran
                                <i class="fas fa-asterisk text-tiny text-red-500"></i>
                            </label>
                            @component('shared.components.input', [
                                'type' => 'number',
                                'name' => 'payment',
                                'placeholder' => 'Masukan total pembayaran...',
                                'extraClassName' => 'payment',
                            ])
                            @endcomponent
                        </div>
                    </div>

                    <button type="submit" class="col-span-2 rounded-full bg-primary text-sm text-white w-1/4 mx-auto px-2 py-2 focus:outline-none">
                        Pesan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@include('admin.modals.fullcalendar-modal')