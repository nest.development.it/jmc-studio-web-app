import Snackbar from 'node-snackbar';
import { Navbar } from './components/Navbar';
import { LoginModal } from './modals/LoginModal';
import { RegisterModal } from './modals/RegisterModal';
import { FullcalendarModal } from './modals/FullcalendarModal';
import { Account } from './pages/Account';
import { Reservation } from './pages/Reservation';
import { Transaction } from './pages/Transaction';
import { PaymentModal } from './modals/PaymentModal';

export const load= () => {

    Navbar();
    LoginModal();
    RegisterModal();
    FullcalendarModal();
    Account();
    Reservation();
    Transaction();
    PaymentModal();
};

load();
