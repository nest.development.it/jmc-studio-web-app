import { disableElement } from '../../utilities/helpers';
var moment = require('moment');
//require('fullcalendar');
import { Calendar } from '@fullcalendar/core';
window.Calendar = Calendar;

import interaction from '@fullcalendar/interaction';
window.interaction = interaction;

import dayGridPlugin from '@fullcalendar/daygrid';
window.dayGridPlugin = dayGridPlugin;

import timeGridPlugin from '@fullcalendar/timegrid';
window.timeGridPlugin = timeGridPlugin;

import listPlugin from '@fullcalendar/list';
window.listPlugin = listPlugin;

import Swal from 'sweetalert2';

export const FullcalendarModal = () => {
    $('#customer-reservation-form .fullcalendar-controlbtn').on('click', function() {
        var selectedProduct = $('#customer-reservation-form .product-dropdown').val();
        if (selectedProduct != null){
            let productSplit = String(selectedProduct).split('$');
            let durationList = String(productSplit[2]).split(':');
            console.log(durationList);

            $('#fullcalendar-modal .modal').removeClass('hidden');

            var eventData = [];
            var eventColor = [];

            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content'),
                }
            });

            $.ajax({
                url: '/calendar/get-events',
                type: 'GET',
                success: (data) => {
                    data['reservation'].forEach(r => {
                        data['transaction'].forEach(t => {
                            data['product'].forEach(p => {
                                if (r['transaction_id'] == t['id']) {
                                    if (t['product_id'] == p['id']) {
                                        if(t['status'] == 0){
                                            eventData.push({
                                                title: String(p['name']),
                                                start: String(r['start_time']),
                                                end: String(r['end_time']),
                                                color: '#ffcd42'
                                            });
                                        }else{
                                            eventData.push({
                                                title: String(p['name']),
                                                start: String(r['start_time']),
                                                end: String(r['end_time']),
                                                color: '#32a852'
                                            });
                                        }

                                    }


                                }
                            });
                        });
                    });

                    data['holidays'].forEach(h => {
                        if(h['isRecurrent'] == 1){
                            console.log(h);
                            eventData.push({
                                title: String(h['name']),
                                startTime: String(h['recurrent_start_time']),
                                endTime: String(h['recurrent_end_time']),
                                daysOfWeek: [h['recurrent_day']],
                                color: '#FF0000'
                            });
                        }else{
                            eventData.push({
                                title: String(h['name']),
                                start: String(h['start_timestamp']),
                                end: String(h['end_timestamp']),
                                color: '#FF0000'
                            });
                        }
                    });

                    //IF COURSE EVENT DATA
                    if(selectedProduct[0] == 1){
                        if($('#customer-reservation-form .course-reservation2').is(":checked")){
                            var startCourse2 = $('#customer-reservation-form .start-date-course2').val();
                            var endCourse2 = $('#customer-reservation-form .end-date-course2').val();

                            console.log(startCourse2);

                            if(startCourse2 != null && endCourse2 != null){
                                eventData.push({
                                    title: "Pertemuan Course ke-2",
                                    start: startCourse2,
                                    end: endCourse2,
                                    color: '#030ea8'
                                });
                            }
                        }

                        if($('#customer-reservation-form .course-reservation3').is(":checked")){
                            var startCourse3 = $('#customer-reservation-form .start-date-course3').val();
                            var endCourse3 = $('#customer-reservation-form .end-date-course3').val();

                            console.log(startCourse3);

                            if(startCourse3 != null && endCourse3 != null){
                                eventData.push({
                                    title: "Pertemuan Course ke-3",
                                    start: startCourse3,
                                    end: endCourse3,
                                    color: '#030ea8'
                                });
                            }
                        }

                        if($('#customer-reservation-form .course-reservation4').is(":checked")){
                            var startCourse4 = $('#customer-reservation-form .start-date-course4').val();
                            var endCourse4 = $('#customer-reservation-form .end-date-course4').val();

                            if(startCourse4 != null && endCourse4 != null){
                                eventData.push({
                                    title: "Pertemuan Course ke-4",
                                    start: startCourse4,
                                    end: endCourse4,
                                    color: '#030ea8'
                                });
                            }
                        }
                    }

                    var calendarEl = $('#fullcalendar-modal .calendar').get(0);
                    //console.log(calendarEl);

                    if(calendarEl != null){
                        console.log(eventData);

                        var calendar = new window.Calendar(calendarEl, {
                            plugins: [window.interaction, window.dayGridPlugin, window.timeGridPlugin, window.listPlugin],
                            selectable: true,
                            initialView: 'dayGridMonth',
                            eventBorderColor: 'white',
                            events: eventData,
                            dateClick: function(info) {
                                console.log(info);
                                if(info.view.type=="dayGridMonth"){
                                    //Pengecekan h+3 reservation
                                    var minimumReservation = null;
                                    if(selectedProduct[0] == 1){
                                        minimumReservation = new Date(new Date().getTime());
                                    }else{
                                        minimumReservation = new Date(new Date().getTime()+(2*24*60*60*1000));
                                    }



                                    if(info.date < minimumReservation){
                                        if(selectedProduct[0] == 1){
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: 'Harap melakukan reservasi setelah hari ini.',
                                            })
                                        }else{
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: 'Harap melakukan reservasi 3 hari sebelum hari-H.',
                                            })
                                        }

                                    }else{
                                        this.changeView("timeGridDay",info.dateStr);
                                    }
                                }else if(info.view.type=="timeGridDay"){
                                    var minimumReservation = null;
                                    if(selectedProduct[0] == 1){
                                        minimumReservation = new Date(new Date().getTime());
                                    }else{
                                        minimumReservation = new Date(new Date().getTime()+(2*24*60*60*1000));
                                    }

                                    if(info.date < minimumReservation){
                                        if(selectedProduct[0] == 1){
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: 'Harap melakukan reservasi setelah hari ini.',
                                            })
                                        }else{
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: 'Harap melakukan reservasi 3 hari sebelum hari-H.',
                                            })
                                        }
                                    }else{
                                        //Check overlap events
                                        let checkStart = new Date(info.date.getTime());
                                        let checkEnd = new Date(info.date.getTime()+(durationList[0]*60*60*1000+durationList[1]*60*1000+durationList[2]*1000));
                                        let checkOverlap = false;

                                        eventData.forEach(e => {
                                            // start-time in between any of the events
                                            if (moment(checkStart).isAfter(e['start']) && moment(checkStart).isBefore(e['end'])) {
                                                console.log("start-time in between any of the events")
                                                checkOverlap =  true;
                                            };
                                            //end-time in between any of the events
                                            if (moment(checkEnd).isAfter(e['start']) && moment(checkEnd).isBefore(e['end'])) {
                                                console.log("end-time in between any of the events")
                                                checkOverlap = true;
                                            }
                                            //any of the events in between/on the start-time and end-time
                                            if (moment(checkStart).isSameOrBefore(e['start']) && moment(checkEnd).isSameOrAfter(e['end'])) {
                                                console.log("any of the events in between/on the start-time and end-time")
                                                checkOverlap =  true;
                                            }
                                        });

                                        console.log(checkOverlap);

                                        if(checkOverlap){
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: 'Jadwal pilihan anda berhalangan dengan reservasi lainnya.',
                                            });
                                        }else{
                                            let sStartTimestamp = moment(checkStart).format('YYYY-MM-DDTHH:mm');
                                            let sEndTimestamp = moment(checkEnd).format('YYYY-MM-DDTHH:mm');;

                                            $('#customer-reservation-form .start-date').val(sStartTimestamp);
                                            $('#customer-reservation-form .end-date').val(sEndTimestamp);

                                            Swal.fire(
                                                'Berhasil!',
                                                'Pilihan tanggal sudah diubah, silahkan tutup kalendar.',
                                                'success'
                                            )
                                        }
                                    }
                                }
                            }

                        });

                        calendar.render();
                    }
                }
            });
        }else{
            Swal.fire(
                'Layanan ?',
                'Silahkan pilih layanan reservasi terlebih dahulu.',
                'question'
            )
        }


    });

    //FULLCALENDAR COURSE 2
    $('#customer-reservation-form .fullcalendar-controlbtn-course2').on('click', function(e){
        var selectedProduct = $('#customer-reservation-form .product-dropdown').val();
        let productSplit = String(selectedProduct).split('$');
        let durationList = String(productSplit[2]).split(':');
        console.log(durationList);

        $('#fullcalendar-modal .modal').removeClass('hidden');

        var eventData = [];
        var eventColor = [];

        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content'),
            }
        });

        $.ajax({
            url: '/calendar/get-events',
            type: 'GET',
            success: (data) => {
                data['reservation'].forEach(r => {
                    data['transaction'].forEach(t => {
                        data['product'].forEach(p => {
                            if (r['transaction_id'] == t['id']) {
                                if (t['product_id'] == p['id']) {
                                    if(t['status'] == 0){
                                        eventData.push({
                                            title: String(p['name']),
                                            start: String(r['start_time']),
                                            end: String(r['end_time']),
                                            color: '#ffcd42'
                                        });
                                    }else{
                                        eventData.push({
                                            title: String(p['name']),
                                            start: String(r['start_time']),
                                            end: String(r['end_time']),
                                            color: '#32a852'
                                        });
                                    }

                                }


                            }
                        });
                    });
                });

                data['holidays'].forEach(h => {
                    if(h['isRecurrent'] == 1){
                        console.log(h);
                        eventData.push({
                            title: String(h['name']),
                            startTime: String(h['recurrent_start_time']),
                            endTime: String(h['recurrent_end_time']),
                            daysOfWeek: [h['recurrent_day']],
                            color: '#FF0000'
                        });
                    }else{
                        eventData.push({
                            title: String(h['name']),
                            start: String(h['start_timestamp']),
                            end: String(h['end_timestamp']),
                            color: '#FF0000'
                        });
                    }
                });

                //getOtherCourseInfo
                var startCourse1 = $('#customer-reservation-form .start-date').val();
                var endCourse1 = $('#customer-reservation-form .end-date').val();

                console.log(startCourse1);

                if(startCourse1 != null && endCourse1 != null){
                    eventData.push({
                        title: "Pertemuan Course ke-1",
                        start: startCourse1,
                        end: endCourse1,
                        color: '#030ea8'
                    });
                }

                if($('#customer-reservation-form .course-reservation3').is(":checked")){
                    var startCourse3 = $('#customer-reservation-form .start-date-course3').val();
                    var endCourse3 = $('#customer-reservation-form .end-date-course3').val();

                    console.log(startCourse3);

                    if(startCourse3 != null && endCourse3 != null){
                        eventData.push({
                            title: "Pertemuan Course ke-3",
                            start: startCourse3,
                            end: endCourse3,
                            color: '#030ea8'
                        });
                    }
                }

                if($('#customer-reservation-form .course-reservation4').is(":checked")){
                    var startCourse4 = $('#customer-reservation-form .start-date-course4').val();
                    var endCourse4 = $('#customer-reservation-form .end-date-course4').val();

                    if(startCourse4 != null && endCourse4 != null){
                        eventData.push({
                            title: "Pertemuan Course ke-4",
                            start: startCourse4,
                            end: endCourse4,
                            color: '#030ea8'
                        });
                    }
                }

                var calendarEl = $('#fullcalendar-modal .calendar').get(0);
                //console.log(calendarEl);

                if(calendarEl != null){
                    console.log(eventData);

                    var calendar = new window.Calendar(calendarEl, {
                        plugins: [window.interaction, window.dayGridPlugin, window.timeGridPlugin, window.listPlugin],
                        selectable: true,
                        initialView: 'dayGridMonth',
                        eventBorderColor: 'white',
                        events: eventData,
                        dateClick: function(info) {
                            console.log(info);
                            if(info.view.type=="dayGridMonth"){
                                //Pengecekan h+3 reservation
                                let minimumReservation = new Date(new Date().getTime());

                                if(info.date < minimumReservation){
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Harap melakukan reservasi setelah hari ini.',
                                    })
                                }else{
                                    this.changeView("timeGridDay",info.dateStr);
                                }
                            }else if(info.view.type=="timeGridDay"){
                                let minimumReservation = new Date(new Date().getTime());


                                if(info.date < minimumReservation){
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Harap melakukan reservasi setelah hari ini.',
                                    });
                                }else{
                                    //Check overlap events
                                    let checkStart = new Date(info.date.getTime());
                                    let checkEnd = new Date(info.date.getTime()+(durationList[0]*60*60*1000+durationList[1]*60*1000+durationList[2]*1000));
                                    let checkOverlap = false;

                                    eventData.forEach(e => {
                                        // start-time in between any of the events
                                        if (moment(checkStart).isAfter(e['start']) && moment(checkStart).isBefore(e['end'])) {
                                            console.log("start-time in between any of the events")
                                            checkOverlap =  true;
                                        };
                                        //end-time in between any of the events
                                        if (moment(checkEnd).isAfter(e['start']) && moment(checkEnd).isBefore(e['end'])) {
                                            console.log("end-time in between any of the events")
                                            checkOverlap = true;
                                        }
                                        //any of the events in between/on the start-time and end-time
                                        if (moment(checkStart).isSameOrBefore(e['start']) && moment(checkEnd).isSameOrAfter(e['end'])) {
                                            console.log("any of the events in between/on the start-time and end-time")
                                            checkOverlap =  true;
                                        }
                                    });

                                    console.log(checkOverlap);

                                    if(checkOverlap){
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Oops...',
                                            text: 'Jadwal pilihan anda berhalangan dengan reservasi lainnya.',
                                        });
                                    }else{
                                        let sStartTimestamp = moment(checkStart).format('YYYY-MM-DDTHH:mm');
                                        let sEndTimestamp = moment(checkEnd).format('YYYY-MM-DDTHH:mm');;

                                        $('#customer-reservation-form .start-date-course2').val(sStartTimestamp);
                                        $('#customer-reservation-form .end-date-course2').val(sEndTimestamp);

                                        Swal.fire(
                                            'Berhasil!',
                                            'Pilihan tanggal sudah diubah, silahkan tutup kalendar.',
                                            'success'
                                        )
                                    }
                                }
                            }
                        }

                    });

                    calendar.render();
                }
            }
        });
    });

    //FULL CALENDAR COURSE 3
    $('#customer-reservation-form .fullcalendar-controlbtn-course3').on('click', function(e){
        var selectedProduct = $('#customer-reservation-form .product-dropdown').val();
        let productSplit = String(selectedProduct).split('$');
        let durationList = String(productSplit[2]).split(':');
        console.log(durationList);

        $('#fullcalendar-modal .modal').removeClass('hidden');

        var eventData = [];
        var eventColor = [];

        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content'),
            }
        });

        $.ajax({
            url: '/calendar/get-events',
            type: 'GET',
            success: (data) => {
                data['reservation'].forEach(r => {
                    data['transaction'].forEach(t => {
                        data['product'].forEach(p => {
                            if (r['transaction_id'] == t['id']) {
                                if (t['product_id'] == p['id']) {
                                    if(t['status'] == 0){
                                        eventData.push({
                                            title: String(p['name']),
                                            start: String(r['start_time']),
                                            end: String(r['end_time']),
                                            color: '#ffcd42'
                                        });
                                    }else{
                                        eventData.push({
                                            title: String(p['name']),
                                            start: String(r['start_time']),
                                            end: String(r['end_time']),
                                            color: '#32a852'
                                        });
                                    }

                                }


                            }
                        });
                    });
                });

                data['holidays'].forEach(h => {
                    if(h['isRecurrent'] == 1){
                        console.log(h);
                        eventData.push({
                            title: String(h['name']),
                            startTime: String(h['recurrent_start_time']),
                            endTime: String(h['recurrent_end_time']),
                            daysOfWeek: [h['recurrent_day']],
                            color: '#FF0000'
                        });
                    }else{
                        eventData.push({
                            title: String(h['name']),
                            start: String(h['start_timestamp']),
                            end: String(h['end_timestamp']),
                            color: '#FF0000'
                        });
                    }
                });

                //getOtherCourseInfo
                var startCourse1 = $('#customer-reservation-form .start-date').val();
                var endCourse1 = $('#customer-reservation-form .end-date').val();

                console.log(startCourse1);

                if(startCourse1 != null && endCourse1 != null){
                    eventData.push({
                        title: "Pertemuan Course ke-1",
                        start: startCourse1,
                        end: endCourse1,
                        color: '#030ea8'
                    });
                }

                if($('#customer-reservation-form .course-reservation2').is(":checked")){
                    var startCourse2 = $('#customer-reservation-form .start-date-course2').val();
                    var endCourse2 = $('#customer-reservation-form .end-date-course2').val();

                    console.log(startCourse2);

                    if(startCourse2 != null && endCourse2 != null){
                        eventData.push({
                            title: "Pertemuan Course ke-2",
                            start: startCourse2,
                            end: endCourse2,
                            color: '#030ea8'
                        });
                    }
                }

                if($('#customer-reservation-form .course-reservation4').is(":checked")){
                    var startCourse4 = $('#customer-reservation-form .start-date-course4').val();
                    var endCourse4 = $('#customer-reservation-form .end-date-course4').val();

                    if(startCourse4 != null && endCourse4 != null){
                        eventData.push({
                            title: "Pertemuan Course ke-4",
                            start: startCourse4,
                            end: endCourse4,
                            color: '#030ea8'
                        });
                    }
                }

                var calendarEl = $('#fullcalendar-modal .calendar').get(0);
                //console.log(calendarEl);

                if(calendarEl != null){
                    console.log(eventData);

                    var calendar = new window.Calendar(calendarEl, {
                        plugins: [window.interaction, window.dayGridPlugin, window.timeGridPlugin, window.listPlugin],
                        selectable: true,
                        initialView: 'dayGridMonth',
                        eventBorderColor: 'white',
                        events: eventData,
                        dateClick: function(info) {
                            console.log(info);
                            if(info.view.type=="dayGridMonth"){
                                //Pengecekan h+3 reservation
                                let minimumReservation = new Date(new Date().getTime());

                                if(info.date < minimumReservation){
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Harap melakukan reservasi setelah hari ini.',
                                    })
                                }else{
                                    this.changeView("timeGridDay",info.dateStr);
                                }
                            }else if(info.view.type=="timeGridDay"){
                                let minimumReservation = new Date(new Date().getTime());


                                if(info.date < minimumReservation){
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Harap melakukan reservasi setelah hari ini.',
                                    });
                                }else{
                                    //Check overlap events
                                    let checkStart = new Date(info.date.getTime());
                                    let checkEnd = new Date(info.date.getTime()+(durationList[0]*60*60*1000+durationList[1]*60*1000+durationList[2]*1000));
                                    let checkOverlap = false;

                                    eventData.forEach(e => {
                                        // start-time in between any of the events
                                        if (moment(checkStart).isAfter(e['start']) && moment(checkStart).isBefore(e['end'])) {
                                            console.log("start-time in between any of the events")
                                            checkOverlap =  true;
                                        };
                                        //end-time in between any of the events
                                        if (moment(checkEnd).isAfter(e['start']) && moment(checkEnd).isBefore(e['end'])) {
                                            console.log("end-time in between any of the events")
                                            checkOverlap = true;
                                        }
                                        //any of the events in between/on the start-time and end-time
                                        if (moment(checkStart).isSameOrBefore(e['start']) && moment(checkEnd).isSameOrAfter(e['end'])) {
                                            console.log("any of the events in between/on the start-time and end-time")
                                            checkOverlap =  true;
                                        }
                                    });

                                    console.log(checkOverlap);

                                    if(checkOverlap){
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Oops...',
                                            text: 'Jadwal pilihan anda berhalangan dengan reservasi lainnya.',
                                        });
                                    }else{
                                        let sStartTimestamp = moment(checkStart).format('YYYY-MM-DDTHH:mm');
                                        let sEndTimestamp = moment(checkEnd).format('YYYY-MM-DDTHH:mm');;

                                        $('#customer-reservation-form .start-date-course3').val(sStartTimestamp);
                                        $('#customer-reservation-form .end-date-course3').val(sEndTimestamp);

                                        Swal.fire(
                                            'Berhasil!',
                                            'Pilihan tanggal sudah diubah, silahkan tutup kalendar.',
                                            'success'
                                        )
                                    }
                                }
                            }
                        }

                    });

                    calendar.render();
                }
            }
        });
    });

    //FULL CALENDAR COURSE 4
    $('#customer-reservation-form .fullcalendar-controlbtn-course4').on('click', function(e){
        var selectedProduct = $('#customer-reservation-form .product-dropdown').val();
        let productSplit = String(selectedProduct).split('$');
        let durationList = String(productSplit[2]).split(':');
        console.log(durationList);

        $('#fullcalendar-modal .modal').removeClass('hidden');

        var eventData = [];
        var eventColor = [];

        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content'),
            }
        });

        $.ajax({
            url: '/calendar/get-events',
            type: 'GET',
            success: (data) => {
                data['reservation'].forEach(r => {
                    data['transaction'].forEach(t => {
                        data['product'].forEach(p => {
                            if (r['transaction_id'] == t['id']) {
                                if (t['product_id'] == p['id']) {
                                    if(t['status'] == 0){
                                        eventData.push({
                                            title: String(p['name']),
                                            start: String(r['start_time']),
                                            end: String(r['end_time']),
                                            color: '#ffcd42'
                                        });
                                    }else{
                                        eventData.push({
                                            title: String(p['name']),
                                            start: String(r['start_time']),
                                            end: String(r['end_time']),
                                            color: '#32a852'
                                        });
                                    }

                                }


                            }
                        });
                    });
                });

                data['holidays'].forEach(h => {
                    if(h['isRecurrent'] == 1){
                        console.log(h);
                        eventData.push({
                            title: String(h['name']),
                            startTime: String(h['recurrent_start_time']),
                            endTime: String(h['recurrent_end_time']),
                            daysOfWeek: [h['recurrent_day']],
                            color: '#FF0000'
                        });
                    }else{
                        eventData.push({
                            title: String(h['name']),
                            start: String(h['start_timestamp']),
                            end: String(h['end_timestamp']),
                            color: '#FF0000'
                        });
                    }
                });

                //getOtherCourseInfo
                var startCourse1 = $('#customer-reservation-form .start-date').val();
                var endCourse1 = $('#customer-reservation-form .end-date').val();

                console.log(startCourse1);

                if(startCourse1 != null && endCourse1 != null){
                    eventData.push({
                        title: "Pertemuan Course ke-1",
                        start: startCourse1,
                        end: endCourse1,
                        color: '#030ea8'
                    });
                }

                if($('#customer-reservation-form .course-reservation2').is(":checked")){
                    var startCourse2 = $('#customer-reservation-form .start-date-course2').val();
                    var endCourse2 = $('#customer-reservation-form .end-date-course2').val();

                    console.log(startCourse2);

                    if(startCourse2 != null && endCourse2 != null){
                        eventData.push({
                            title: "Pertemuan Course ke-2",
                            start: startCourse2,
                            end: endCourse2,
                            color: '#030ea8'
                        });
                    }
                }

                if($('#customer-reservation-form .course-reservation3').is(":checked")){
                    var startCourse3 = $('#customer-reservation-form .start-date-course3').val();
                    var endCourse3 = $('#customer-reservation-form .end-date-course3').val();

                    if(startCourse3 != null && endCourse3 != null){
                        eventData.push({
                            title: "Pertemuan Course ke-3",
                            start: startCourse3,
                            end: endCourse3,
                            color: '#030ea8'
                        });
                    }
                }

                var calendarEl = $('#fullcalendar-modal .calendar').get(0);
                //console.log(calendarEl);

                if(calendarEl != null){
                    console.log(eventData);

                    var calendar = new window.Calendar(calendarEl, {
                        plugins: [window.interaction, window.dayGridPlugin, window.timeGridPlugin, window.listPlugin],
                        selectable: true,
                        initialView: 'dayGridMonth',
                        eventBorderColor: 'white',
                        events: eventData,
                        dateClick: function(info) {
                            console.log(info);
                            if(info.view.type=="dayGridMonth"){
                                //Pengecekan h+3 reservation
                                let minimumReservation = new Date(new Date().getTime());

                                if(info.date < minimumReservation){
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Harap melakukan reservasi setelah hari ini.',
                                    })
                                }else{
                                    this.changeView("timeGridDay",info.dateStr);
                                }
                            }else if(info.view.type=="timeGridDay"){
                                let minimumReservation = new Date(new Date().getTime());


                                if(info.date < minimumReservation){
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Harap melakukan reservasi setelah hari ini.',
                                    });
                                }else{
                                    //Check overlap events
                                    let checkStart = new Date(info.date.getTime());
                                    let checkEnd = new Date(info.date.getTime()+(durationList[0]*60*60*1000+durationList[1]*60*1000+durationList[2]*1000));
                                    let checkOverlap = false;

                                    eventData.forEach(e => {
                                        // start-time in between any of the events
                                        if (moment(checkStart).isAfter(e['start']) && moment(checkStart).isBefore(e['end'])) {
                                            console.log("start-time in between any of the events")
                                            checkOverlap =  true;
                                        };
                                        //end-time in between any of the events
                                        if (moment(checkEnd).isAfter(e['start']) && moment(checkEnd).isBefore(e['end'])) {
                                            console.log("end-time in between any of the events")
                                            checkOverlap = true;
                                        }
                                        //any of the events in between/on the start-time and end-time
                                        if (moment(checkStart).isSameOrBefore(e['start']) && moment(checkEnd).isSameOrAfter(e['end'])) {
                                            console.log("any of the events in between/on the start-time and end-time")
                                            checkOverlap =  true;
                                        }
                                    });

                                    console.log(checkOverlap);

                                    if(checkOverlap){
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Oops...',
                                            text: 'Jadwal pilihan anda berhalangan dengan reservasi lainnya.',
                                        });
                                    }else{
                                        let sStartTimestamp = moment(checkStart).format('YYYY-MM-DDTHH:mm');
                                        let sEndTimestamp = moment(checkEnd).format('YYYY-MM-DDTHH:mm');;

                                        $('#customer-reservation-form .start-date-course4').val(sStartTimestamp);
                                        $('#customer-reservation-form .end-date-course4').val(sEndTimestamp);

                                        Swal.fire(
                                            'Berhasil!',
                                            'Pilihan tanggal sudah diubah, silahkan tutup kalendar.',
                                            'success'
                                        )
                                    }
                                }
                            }
                        }

                    });

                    calendar.render();
                }
            }
        });
    });

    //FULL CALENDAR CHECK SOUND
    $('#customer-reservation-form .fullcalendar-controlbtn-streaming').on('click', function(e){
        var selectedProduct = $('#customer-reservation-form .product-dropdown').val();
        let productSplit = String(selectedProduct).split('$');
        let durationList = String(productSplit[2]).split(':');
        console.log(durationList);

        $('#fullcalendar-modal .modal').removeClass('hidden');

        var eventData = [];
        var eventColor = [];

        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content'),
            }
        });

        $.ajax({
            url: '/calendar/get-events',
            type: 'GET',
            success: (data) => {
                data['reservation'].forEach(r => {
                    data['transaction'].forEach(t => {
                        data['product'].forEach(p => {
                            if (r['transaction_id'] == t['id']) {
                                if (t['product_id'] == p['id']) {
                                    if(t['status'] == 0){
                                        eventData.push({
                                            title: String(p['name']),
                                            start: String(r['start_time']),
                                            end: String(r['end_time']),
                                            color: '#ffcd42'
                                        });
                                    }else{
                                        eventData.push({
                                            title: String(p['name']),
                                            start: String(r['start_time']),
                                            end: String(r['end_time']),
                                            color: '#32a852'
                                        });
                                    }

                                }


                            }
                        });
                    });
                });

                data['holidays'].forEach(h => {
                    if(h['isRecurrent'] == 1){
                        console.log(h);
                        eventData.push({
                            title: String(h['name']),
                            startTime: String(h['recurrent_start_time']),
                            endTime: String(h['recurrent_end_time']),
                            daysOfWeek: [h['recurrent_day']],
                            color: '#FF0000'
                        });
                    }else{
                        eventData.push({
                            title: String(h['name']),
                            start: String(h['start_timestamp']),
                            end: String(h['end_timestamp']),
                            color: '#FF0000'
                        });
                    }
                });

                //getOtherCourseInfo
                var startCourse1 = $('#customer-reservation-form .start-date').val();
                var endCourse1 = $('#customer-reservation-form .end-date').val();

                console.log(startCourse1);

                if(startCourse1 != null && endCourse1 != null){
                    eventData.push({
                        title: "Rencana Jadwal Live Streaming",
                        start: startCourse1,
                        end: endCourse1,
                        color: '#030ea8'
                    });
                }

                var calendarEl = $('#fullcalendar-modal .calendar').get(0);
                //console.log(calendarEl);

                if(calendarEl != null){
                    console.log(eventData);

                    var calendar = new window.Calendar(calendarEl, {
                        plugins: [window.interaction, window.dayGridPlugin, window.timeGridPlugin, window.listPlugin],
                        selectable: true,
                        initialView: 'dayGridMonth',
                        eventBorderColor: 'white',
                        events: eventData,
                        dateClick: function(info) {
                            console.log(info);
                            if(info.view.type=="dayGridMonth"){
                                //Pengecekan h+3 reservation
                                let minimumReservation = new Date(new Date().getTime());

                                if(info.date < minimumReservation){
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Harap melakukan reservasi setelah hari ini.',
                                    })
                                }else{
                                    this.changeView("timeGridDay",info.dateStr);
                                }
                            }else if(info.view.type=="timeGridDay"){
                                let minimumReservation = new Date(new Date().getTime());


                                if(info.date < minimumReservation){
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Harap melakukan reservasi setelah hari ini.',
                                    });
                                }else{
                                    //Check overlap events
                                    let checkStart = new Date(info.date.getTime());
                                    let checkEnd = new Date(info.date.getTime()+(durationList[0]*60*60*1000+durationList[1]*60*1000+durationList[2]*1000));
                                    let checkOverlap = false;

                                    eventData.forEach(e => {
                                        // start-time in between any of the events
                                        if (moment(checkStart).isAfter(e['start']) && moment(checkStart).isBefore(e['end'])) {
                                            console.log("start-time in between any of the events")
                                            checkOverlap =  true;
                                        };
                                        //end-time in between any of the events
                                        if (moment(checkEnd).isAfter(e['start']) && moment(checkEnd).isBefore(e['end'])) {
                                            console.log("end-time in between any of the events")
                                            checkOverlap = true;
                                        }
                                        //any of the events in between/on the start-time and end-time
                                        if (moment(checkStart).isSameOrBefore(e['start']) && moment(checkEnd).isSameOrAfter(e['end'])) {
                                            console.log("any of the events in between/on the start-time and end-time")
                                            checkOverlap =  true;
                                        }
                                    });

                                    console.log(checkOverlap);

                                    if(checkOverlap){
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Oops...',
                                            text: 'Jadwal pilihan anda berhalangan dengan reservasi lainnya.',
                                        });
                                    }else{
                                        let sStartTimestamp = moment(checkStart).format('YYYY-MM-DDTHH:mm');
                                        let sEndTimestamp = moment(checkEnd).format('YYYY-MM-DDTHH:mm');;

                                        $('#customer-reservation-form .start-date-streaming').val(sStartTimestamp);
                                        $('#customer-reservation-form .end-date-streaming').val(sEndTimestamp);

                                        Swal.fire(
                                            'Berhasil!',
                                            'Pilihan tanggal sudah diubah, silahkan tutup kalendar.',
                                            'success'
                                        )
                                    }
                                }
                            }
                        }

                    });

                    calendar.render();
                }
            }
        });
    });
};
