import { disableElement } from '../../utilities/helpers';

export const LoginModal= () => {
    disableElement('#login-form button[type="submit"]', true);

    $('#login-form input').on('input', function() {
        let isEmpty= true;

        $('#login-form input').each(function() {
            isEmpty= $(this).val() === '';
        });

        if (isEmpty) {
            disableElement('#login-form button[type="submit"]', true);
        } else {
            disableElement('#login-form button[type="submit"]', false);
        }
    });
    $('#login-form').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: '/auth/login',
            type: 'POST',
            data: new FormData($(this)[0]),
            success: (res) => {
                $(this).trigger('reset');
                window.location.replace(res.redirectTo);
            }
        })
    });
};
