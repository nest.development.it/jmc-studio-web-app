import { disableElement } from '../../utilities/helpers';

export const RegisterModal= () => {
    disableElement('#register-form button[type="submit"]', true);

    $('#register-form input').on('input', function() {
        let isEmpty= true;

        $('#register-form input').each(function() {
            isEmpty= $(this).val() === '';
        });

        if (isEmpty) {
            disableElement('#register-form button[type="submit"]', true);
        } else {
            disableElement('#register-form button[type="submit"]', false);
        }
    });
    $('#register-form').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: '/auth/register',
            type: 'POST',
            data: new FormData($(this)[0]),
            success: () => {
                $(this).trigger('reset');
            }
        });
    });
};
