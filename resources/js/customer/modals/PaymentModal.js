import { disableElement } from '../../utilities/helpers';
import Swal from 'sweetalert2';
import Snackbar from 'node-snackbar';

export const PaymentModal = () => {
    $(document).on('click','#customer-transaction-payment', function(e){
        e.preventDefault();
        var transactionId = $(this).val();

        $('#customer-payment-modal .modal').removeClass('hidden');
        $('#customer-payment-modal .input-transaction-id').val(transactionId);
    });

    $('#form-customer-payment').on('submit', function(e){
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "Pastikan data terisi dengan benar!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, bayar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                    });
                    $.ajax({
                        url: '/transaction/upload-payment',
                        type: 'POST',
                        data: new FormData($(this)[0]),
                        success: (data) => {
                            //$(this).trigger('reset');
                            Swal.fire(
                                'Saved!',
                                'Pembayaran sudah tersimpan. Harap menunggu konfirmasi dari admin.',
                                'success'
                            )
                            $('.modal').addClass('hidden');
                            console.log(data);

                            //Update datatable
                            var table = $('#yajra-datatable-customer-transaction').DataTable();
                            table.ajax.reload(null, false);
                        },
                        error: () => {
                            $(document).ajaxComplete(async function(event, res, options) {
                                Swal.fire(
                                    'Error!',
                                    'File bukti transfer tidak sesuai ketentuan.',
                                    'error'
                                )
                            });
                        }
                    });
                }
        });

        return false;
    });
};
