import { split } from 'lodash';
import Snackbar from 'node-snackbar';
import Swal from 'sweetalert2';
import { disableElement } from '../../utilities/helpers';

export const Transaction = () => {
    const getTransactionHistory = () => {
        $('#yajra-datatable-customer-transaction').DataTable({
            processing: true,
            serverSide: false,
            ajax: "/transaction/get-history",
            columns: [
              {data: 'id', name: 'id'},
              {data: 'productName', name: 'product_id'},
              {data: 'priceFormat', name: 'price'},
              {data: 'detail', name: 'detail', orderable: false, searchable: false},
              {data: 'status_btn', name: 'status_btn', orderable: false, searchable: false},
            ],
        });
    };

    const getTransactionDetail = () => {
        $(document).on('click','#customer-transaction-detail', function(e){
            e.preventDefault();
            var transactionId = $(this).val();
            console.log(transactionId);

            $('#customer-transaction-modal .modal').removeClass('hidden');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                processData: false,
                contentType: false,
                dataType: 'JSON',
            });
            $.ajax({
                url: 'transaction/action/get-detail/'+transactionId,
                type: 'GET',
                success: (data) => {
                    console.log(data);
                    $('#customer-transaction-modal .namaProduk').html(data['product']['name']);
                    $('#customer-transaction-modal .durasi').html(data['product']['duration']);
                    $('#customer-transaction-modal .total').html(data['totalHarga']);
                    var reservationListTanggal = "";
                    var reservationListWaktu = "";
                    var ctr = 1;
                    data['reservation'].forEach(r => {
                        var startSplit = split(r['start_time'],' ');
                        var endSplit = split(r['end_time'],' ');
                        reservationListTanggal += "<h1>"+ctr+" | "+startSplit[0]+"</h1>";
                        reservationListWaktu += "<h1>"+ctr+" | "+startSplit[1]+" - "+endSplit[1]+"</h1>";
                        ctr++;
                    });
                    $('#customer-transaction-modal .tanggal').html(reservationListTanggal);
                    $('#customer-transaction-modal .waktu').html(reservationListWaktu);
                },
                error: () => {

                }
            });
        });
    }

    getTransactionDetail();
    getTransactionHistory();
};
