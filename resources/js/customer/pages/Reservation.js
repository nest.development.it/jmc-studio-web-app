import { split } from 'lodash';
import Snackbar from 'node-snackbar';
import Swal from 'sweetalert2';
import { disableElement } from '../../utilities/helpers';

export const Reservation = () => {

    const onChangeProduct = () => {
        $('#customer-reservation-form .input-product').on('change', function(e){
            e.preventDefault();
            var selected = split($(this).val(),'$')[0];

            if(selected == 1){
                $('#customer-reservation-form .course-reservation').removeClass('hidden');
                $('#customer-reservation-form .streaming-reservation').addClass('hidden');
            }else if(selected == 2){
                $('#customer-reservation-form .course-reservation').addClass('hidden');
                $('#customer-reservation-form .streaming-reservation').removeClass('hidden');
            }else{
                $('#customer-reservation-form .course-reservation').addClass('hidden');
                $('#customer-reservation-form .streaming-reservation').addClass('hidden');
            }

            $("#customer-reservation-form input[type=datetime-local]").val("");
            $("#customer-reservation-form input[type=checkbox]").prop('checked',false);
        });
    }

    const onChangeCourseReservation = () => {
        $('#customer-reservation-form .course-reservation2').on('change', function(e){
            e.preventDefault();

            var startUtama = $('#customer-reservation-form .start-date').val();
            var endUtama = $('#customer-reservation-form .end-date').val();
            console.log(startUtama);
            if(startUtama == "" || endUtama == ""){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Wajib mengisi pertemuan utama/pertama terlebih dahulu!',
                });
                $(this).prop('checked',false);
            }else{
                if($(this).is(":checked")){
                    $('#customer-reservation-form .fullcalendar-controlbtn-course2').prop('disabled',false);
                }else{
                    $('#customer-reservation-form .start-date-course2').val("");
                    $('#customer-reservation-form .end-date-course2').val("");
                    $('#customer-reservation-form .fullcalendar-controlbtn-course2').prop('disabled',true);
                }
            }
        });

        $('#customer-reservation-form .course-reservation3').on('change', function(e){
            e.preventDefault();
            var startUtama = $('#customer-reservation-form .start-date').val();
            var endUtama = $('#customer-reservation-form .end-date').val();
            console.log(startUtama);
            if(startUtama == "" || endUtama == ""){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Wajib mengisi pertemuan utama/pertama terlebih dahulu!',
                });
                $(this).prop('checked',false);
            }else{
                if($(this).is(":checked")){
                    $('#customer-reservation-form .fullcalendar-controlbtn-course3').prop('disabled',false);
                }else{
                    $('#customer-reservation-form .start-date-course3').val("");
                    $('#customer-reservation-form .end-date-course3').val("");
                    $('#customer-reservation-form .fullcalendar-controlbtn-course3').prop('disabled',true);
                }
            }
        });

        $('#customer-reservation-form .course-reservation4').on('change', function(e){
            e.preventDefault();
            var startUtama = $('#customer-reservation-form .start-date').val();
            var endUtama = $('#customer-reservation-form .end-date').val();
            console.log(startUtama);
            if(startUtama == "" || endUtama == ""){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Wajib mengisi pertemuan utama/pertama terlebih dahulu!',
                });
                $(this).prop('checked',false);
            }else{
                if($(this).is(":checked")){
                    $('#customer-reservation-form .fullcalendar-controlbtn-course4').prop('disabled',false);
                }else{
                    $('#customer-reservation-form .start-date-course4').val("");
                    $('#customer-reservation-form .end-date-course4').val("");
                    $('#customer-reservation-form .fullcalendar-controlbtn-course4').prop('disabled',true);
                }
            }
        });
    }

    const onChangeStreamingSound = () => {
        $('#customer-reservation-form .streaming-checksound').on('change', function(e){
            e.preventDefault();
            var startUtama = $('#customer-reservation-form .start-date').val();
            var endUtama = $('#customer-reservation-form .end-date').val();
            console.log(startUtama);
            if(startUtama == "" || endUtama == ""){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Wajib mengisi pertemuan utama/pertama terlebih dahulu!',
                });
                $(this).prop('checked',false);
            }else{
                if($(this).is(":checked")){
                    $('#customer-reservation-form .fullcalendar-controlbtn-streaming').prop('disabled',false);
                }else{
                    $('#customer-reservation-form .start-date-streaming').val("");
                    $('#customer-reservation-form .end-date-streaming').val("");
                    $('#customer-reservation-form .fullcalendar-controlbtn-streaming').prop('disabled',true);
                }
            }
        });
    }

    const reservationInput = () => {
        $('#customer-reservation-form .product-dropdown').on('change', function(){
            const productVal = String($(this).val()).split("$");
            const price = productVal[1];
            console.log(price);
            $('#customer-reservation-form .product-price').val(price);
            console.log($('#customer-reservation-form .product-price').val());
        });

        $('#customer-reservation-form').on('submit', function(e){
            e.preventDefault();
            Swal.fire({
                title: 'Are you sure?',
                text: "Pastikan data terisi dengan benar!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, order it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            processData: false,
                            contentType: false,
                            dataType: 'JSON',
                        });
                        $.ajax({
                            url: '/transaction/new-reservation',
                            type: 'POST',
                            data: new FormData($(this)[0]),
                            success: (data) => {
                                //$(this).trigger('reset');
                                console.log(data);
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Tersimpan!',
                                    text: 'Harap melanjutkan pembayaran pada histori transaksi.',
                                });
                            },
                            error: function (request, status, error) {
                                console.log(request);
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: JSON.parse(request.responseText).message,
                                });
                            }
                        });
                    }
            });

            return false;
        });
    }

    reservationInput();
    onChangeProduct();
    onChangeStreamingSound();
    onChangeCourseReservation();
};
