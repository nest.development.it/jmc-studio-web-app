import Snackbar from 'node-snackbar';
import { disableElement } from '../../utilities/helpers';

export const Account= () => {
    console.log('test push');

    const updateAccount= () => {
        disableElement('#update-account-form button[type="submit"]', true);

        $('#update-account-form input').on('input', function() {
            let isEmpty= true;

            $('#update-account-form input').each(function() {
                isEmpty= $(this).val() === '';
            });

            if (isEmpty) {
                disableElement('#update-account-form button[type="submit"]', true);
            } else {
                disableElement('#update-account-form button[type="submit"]', false);
            }
        });
        $('#update-account-form').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                url: '/customer/update-account',
                type: 'POST',
                data: new FormData($(this)[0])
            });
        });
    }

    const changePassword= () => {
        disableElement('#change-password-form button[type="submit"]', true);

        $('#change-password-form input').on('input', function() {
            let isEmpty= true;

            $('#change-password-form input').each(function() {
                isEmpty= $(this).val() === '';
            });

            if (isEmpty) {
                disableElement('#change-password-form button[type="submit"]', true);
            } else {
                disableElement('#change-password-form button[type="submit"]', false);
            }
        });
        $('#change-password-form').on('submit', function(e) {
            e.preventDefault();

            const formData= new FormData($(this)[0]);

            if (formData.get('new_password') !== formData.get('confirm_new_password')) {
                Snackbar.show({
                    text: 'Password dan konfirmasi password tidak sama.',
                    actionText: 'Tutup',
                    duration: 2000,
                    pos: 'bottom-center',
                });
            } else {
                $.ajax({
                    url: '/customer/change-password',
                    type: 'POST',
                    data: formData,
                    success: () => {
                        $(this).trigger('reset');
                    }
                });
            }
        });
    }

    updateAccount();
    changePassword();
};
