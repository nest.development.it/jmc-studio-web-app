import { AdminAccount } from './pages/AdminAccount';
import { AdminProduct } from './pages/AdminProduct';
import { AdminTransaction } from './pages/AdminTransaction';
import { AdminHoliday } from './pages/AdminHoliday';
import { FullcalendarModal } from './modals/FullcalendarModal';

export const load = () => {
    AdminAccount();
    AdminProduct();
    AdminHoliday();
    AdminTransaction();
    FullcalendarModal();
};

load();
