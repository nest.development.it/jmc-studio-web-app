import { disableElement } from '../../utilities/helpers';
var moment = require('moment');
//require('fullcalendar');
import { Calendar } from '@fullcalendar/core';
window.Calendar = Calendar;

import interaction from '@fullcalendar/interaction';
window.interaction = interaction;

import dayGridPlugin from '@fullcalendar/daygrid';
window.dayGridPlugin = dayGridPlugin;

import timeGridPlugin from '@fullcalendar/timegrid';
window.timeGridPlugin = timeGridPlugin;

import listPlugin from '@fullcalendar/list';
window.listPlugin = listPlugin;

import Swal from 'sweetalert2';

export const FullcalendarModal = () => {
    $('#admin-cashier-form .fullcalendar-controlbtn').on('click', function() {
        var selectedProduct = $('#admin-cashier-form .product-dropdown').val();
        if (selectedProduct != null){
            let productSplit = String(selectedProduct).split('$');
            let durationList = String(productSplit[2]).split(':');
            console.log(durationList);

            $('#fullcalendar-modal .modal').removeClass('hidden');

            var eventData = [];
            var eventColor = [];

            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content'),
                }
            });

            $.ajax({
                url: '/calendar/get-events',
                type: 'GET',
                success: (data) => {
                    data['reservation'].forEach(r => {
                        data['transaction'].forEach(t => {
                            data['product'].forEach(p => {
                                if (r['transaction_id'] == t['id']) {
                                    if (t['product_id'] == p['id']) {
                                        if(t['status'] == 0){
                                            eventData.push({
                                                title: String(p['name']),
                                                start: String(r['start_time']),
                                                end: String(r['end_time']),
                                                color: '#ffcd42'
                                            });
                                        }else{
                                            eventData.push({
                                                title: String(p['name']),
                                                start: String(r['start_time']),
                                                end: String(r['end_time']),
                                                color: '#32a852'
                                            });
                                        }

                                    }


                                }
                            });
                        });
                    });

                    data['holidays'].forEach(h => {
                        if(h['isRecurrent'] == 1){
                            console.log(h);
                            eventData.push({
                                title: String(h['name']),
                                startTime: String(h['recurrent_start_time']),
                                endTime: String(h['recurrent_end_time']),
                                daysOfWeek: [h['recurrent_day']],
                                color: '#FF0000'
                            });
                        }else{
                            eventData.push({
                                title: String(h['name']),
                                start: String(h['start_timestamp']),
                                end: String(h['end_timestamp']),
                                color: '#FF0000'
                            });
                        }
                    });

                    var calendarEl = $('#fullcalendar-modal .calendar').get(0);
                    //console.log(calendarEl);

                    if(calendarEl != null){
                        console.log(eventData);

                        var calendar = new window.Calendar(calendarEl, {
                            plugins: [window.interaction, window.dayGridPlugin, window.timeGridPlugin, window.listPlugin],
                            selectable: true,
                            initialView: 'dayGridMonth',
                            eventBorderColor: 'white',
                            events: eventData,
                            dateClick: function(info) {
                                console.log(info);
                                if(info.view.type=="dayGridMonth"){
                                    //Pengecekan h+3 reservation
                                    let minimumReservation = new Date(new Date().getTime()+(2*24*60*60*1000));

                                    if(info.date < minimumReservation){
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Oops...',
                                            text: 'Harap melakukan reservasi 3 hari sebelum hari-H.',
                                        })
                                    }else{
                                        this.changeView("timeGridDay",info.dateStr);
                                    }
                                }else if(info.view.type=="timeGridDay"){
                                    let minimumReservation = new Date(new Date().getTime()+(2*24*60*60*1000));


                                    if(info.date < minimumReservation){
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Oops...',
                                            text: 'Harap melakukan reservasi 3 hari sebelum hari-H.',
                                        });
                                    }else{
                                        //Check overlap events
                                        let checkStart = new Date(info.date.getTime());
                                        let checkEnd = new Date(info.date.getTime()+(durationList[0]*60*60*1000+durationList[1]*60*1000+durationList[2]*1000));
                                        let checkOverlap = false;

                                        eventData.forEach(e => {
                                            // start-time in between any of the events
                                            if (moment(checkStart).isAfter(e['start']) && moment(checkStart).isBefore(e['end'])) {
                                                console.log("start-time in between any of the events")
                                                checkOverlap =  true;
                                            };
                                            //end-time in between any of the events
                                            if (moment(checkEnd).isAfter(e['start']) && moment(checkEnd).isBefore(e['end'])) {
                                                console.log("end-time in between any of the events")
                                                checkOverlap = true;
                                            }
                                            //any of the events in between/on the start-time and end-time
                                            if (moment(checkStart).isSameOrBefore(e['start']) && moment(checkEnd).isSameOrAfter(e['end'])) {
                                                console.log("any of the events in between/on the start-time and end-time")
                                                checkOverlap =  true;
                                            }
                                        });

                                        console.log(checkOverlap);

                                        if(checkOverlap){
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: 'Jadwal pilihan anda berhalangan dengan reservasi lainnya.',
                                            });
                                        }else{
                                            let sStartTimestamp = moment(checkStart).format('YYYY-MM-DDTHH:mm');
                                            let sEndTimestamp = moment(checkEnd).format('YYYY-MM-DDTHH:mm');;

                                            $('#admin-cashier-form .start-date').val(sStartTimestamp);
                                            $('#admin-cashier-form .end-date').val(sEndTimestamp);

                                            Swal.fire(
                                                'Berhasil!',
                                                'Pilihan tanggal sudah diubah, silahkan tutup kalendar.',
                                                'success'
                                            )
                                        }
                                    }
                                }
                            }

                        });

                        calendar.render();
                    }
                }
            });
        }else{
            Swal.fire(
                'Layanan ?',
                'Silahkan pilih layanan reservasi terlebih dahulu.',
                'question'
            )
        }


    });
};
