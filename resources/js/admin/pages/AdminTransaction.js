import Swal from 'sweetalert2';
import { split } from 'lodash';

export const AdminTransaction = () => {
    console.log("loading adminTransaction JS");

    const getTransactionApproval = () => {
        /*
        $('#admin-master-account').ready(function () {
            console.log("Taking Data...");
            var table = $('#admin-master-account .yajra-datatable ').DataTable({
                processing: true,
                serverSide: true,
                ajax: "/user/action/get_list_customer",
                columns: [
                  {data: 'id', name: 'id'},
                  {data: 'name', name: 'name'},
                  {data: 'email', name: 'email'},
                  {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
            });
            console.log(table);
        });
        */

        $('#yajra-datatable-transaction-approval').DataTable({
            processing: true,
            serverSide: false,
            ajax: "/get-transaction-approval",
            columns: [
              {data: 'id', name: 'id'},
              {data: 'user_name', name: 'user_name'},
              {data: 'user_email', name: 'user_email'},
              {data: 'product_name', name: 'product_name'},
              {data: 'price_format', name: 'price_format'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
        });
    }

    const approveTransaction = () => {
        $(document).on('submit','#admin-transaction-approval', function(e){
            e.preventDefault();

            Swal.fire({
                title: 'Are you sure?',
                text: "Anda akan memverifikasi bukti pembayaran ini!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, save it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Saved!',
                        'Transaksi sudah diselesaikan.',
                        'success'
                    )

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                    });
                    $.ajax({
                        url: '/transaction/action/approve/',
                        type: 'POST',
                        data: new FormData($(this)[0]),
                        success: (data) => {
                            //$(this).trigger('reset');
                            console.log(data);

                            var table = $('#yajra-datatable-transaction-approval').DataTable();
                            table.ajax.reload(null, false);
                        }
                    });
                }
            });

            return false;
        });
    }

    const cashierTransaction = () => {
        $('#admin-cashier-form .product-dropdown').on('change', function(){
            const productVal = String($(this).val()).split("$");
            const price = productVal[1];
            console.log(price);
            $('#admin-cashier-form .product-price').val(price);
            console.log($('#admin-cashier-form .product-price').val());
        });

        $('#admin-cashier-form').on('submit', function(e){
            e.preventDefault();

            Swal.fire({
                title: 'Are you sure?',
                text: "Pastikan data terisi dengan benar!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, order it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            processData: false,
                            contentType: false,
                            dataType: 'JSON',
                        });
                        $.ajax({
                            url: '/transaction/action/cashier-input',
                            type: 'POST',
                            data: new FormData($(this)[0]),
                            success: (data) => {
                                //$(this).trigger('reset');
                                console.log(data);
                                Swal.fire({
                                    title: 'Are you sure?',
                                    text: "Anda akan menyimpan transaksi ini!",
                                    icon: 'success',
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Yes, save it!'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        location.reload();
                                    }
                                });
                            }
                        });
                    }
            });

            return false;
        });
    }

    const showTransactionPayment = () => {
        $(document).on('click', '#admin-transaction-approval .payment-image', function(e){
            e.preventDefault();

            $('#payment-modal .modal').removeClass('hidden');

            var thisValue = $(this).val().split('|');
            console.log(thisValue);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                processData: false,
                contentType: false,
                dataType: 'JSON',
            });
            $.ajax({
                url: '/transaction/action/get-payment-image/'+thisValue[0],
                type: 'GET',
                success: (data) => {
                    //$(this).trigger('reset');
                    console.log(data);
                    $('#payment-modal .image-container').prop('src',data);
                    $('#payment-modal .payment-timestamp').html(thisValue[1]);
                }
            });

            console.log($(this).val());
        });
    }

    const getTransactionReport = () => {
        $(document).ready(function(){
            var today = new Date().toISOString().split('T')[0];
            $('#admin-transaction-report .startDate').val(today);
            $('#admin-transaction-report .endDate').val(today);
        });

        $('#yajra-datatable-master-transaction').DataTable({
            processing: true,
            serverSide: false,
            ajax: "transaction/action/get-report",
            columns: [
              {data: 'id', name: 'id'},
              {data: 'createdDateFormat', name: 'createdDateFormat'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
        });
    }

    const filterReportDate = () => {
        $('#admin-transaction-report').on('submit', function(e){
            e.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                processData: false,
                contentType: false,
                dataType: 'JSON',
            });
            $.ajax({
                url: '/transaction/action/update-report/',
                type: 'POST',
                data: new FormData($(this)[0]),
                success: (data) => {
                    //$(this).trigger('reset');
                    var table = $('#yajra-datatable-master-transaction').DataTable();
                    table.ajax.reload(null, false);
                }
            });
        });
    }

    const showTransactionDetail = () => {
        $(document).on('click','.admin-detail-transaction', function(e){
            e.preventDefault();
            var transactionId = $(this).val();
            console.log(transactionId);

            $('#admin-transaction-modal .modal').removeClass('hidden');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                processData: false,
                contentType: false,
                dataType: 'JSON',
            });
            $.ajax({
                url: 'transaction/action/get-detail/'+transactionId,
                type: 'GET',
                success: (data) => {
                    console.log(data);
                    $('#admin-transaction-modal .namaProduk').html(data['product']['name']);
                    $('#admin-transaction-modal .durasi').html(data['product']['duration']);
                    $('#admin-transaction-modal .total').html(data['totalHarga']);
                    var reservationListTanggal = "";
                    var reservationListWaktu = "";
                    var ctr = 1;
                    data['reservation'].forEach(r => {
                        var startSplit = split(r['start_time'],' ');
                        var endSplit = split(r['end_time'],' ');
                        reservationListTanggal += "<h1>"+ctr+" | "+startSplit[0]+"</h1>";
                        reservationListWaktu += "<h1>"+ctr+" | "+startSplit[1]+" - "+endSplit[1]+"</h1>";
                        ctr++;
                    });
                    $('#admin-transaction-modal .tanggal').html(reservationListTanggal);
                    $('#admin-transaction-modal .waktu').html(reservationListWaktu);
                },
                error: () => {

                }
            });
        });
    }

    showTransactionPayment();
    filterReportDate();
    getTransactionReport();
    cashierTransaction();
    getTransactionApproval();
    approveTransaction();
    showTransactionDetail();
};
