import _ from 'lodash';
import Swal from 'sweetalert2';

export const AdminProduct = () => {
    console.log("loading adminProduct JS");

    const getProduct = () => {
        /*
        $('#admin-master-account').ready(function () {
            console.log("Taking Data...");
            var table = $('#admin-master-account .yajra-datatable ').DataTable({
                processing: true,
                serverSide: true,
                ajax: "/user/action/get_list_customer",
                columns: [
                  {data: 'id', name: 'id'},
                  {data: 'name', name: 'name'},
                  {data: 'email', name: 'email'},
                  {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
            });
            console.log(table);
        });
        */

        $('#yajra-datatable-master-product').DataTable({
            processing: true,
            serverSide: false,
            ajax: "/product/action/get_list_product",
            columns: [
              {data: 'id', name: 'id'},
              {data: 'name', name: 'name'},
              {data: 'duration', name: 'duration'},
              {data: 'price', name: 'price'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
        });
    }

    const adminDeleteProduct = () => {
        $(document).on('submit','#btn-product-delete', function(e){
            e.preventDefault();

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {


                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            processData: false,
                            contentType: false,
                            dataType: 'JSON',
                        });
                        $.ajax({
                            url: '/product/action/delete/',
                            type: 'POST',
                            data: new FormData($(this)[0]),
                            success: (data) => {
                                //$(this).trigger('reset');
                                console.log(data);
                                Swal.fire(
                                    'Deleted!',
                                    'Your file has been deleted.',
                                    'success'
                                )
                                var table = $('#yajra-datatable-master-product').DataTable();
                                table.ajax.reload(null, false);
                            },
                            error: function (request, status, error) {
                                console.log(request);
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: JSON.parse(request.responseText).message,
                                });
                            }

                        });
                    }
            });

            return false;
        });
    }

    const adminUpdateProduct = () => {
        $('#admin-product-edit').on('submit', function(e){
            e.preventDefault();
            var productId = String($('#admin-product-edit .productId').val());
            var url = '/product/action/edit/'+productId;
            console.log(url);
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, update it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            processData: false,
                            contentType: false,
                            dataType: 'JSON',
                        });
                        $.ajax({
                            url: '/product/action/edit/'+productId,
                            type: 'POST',
                            data: new FormData($(this)[0]),
                            success: (data) => {
                                //$(this).trigger('reset');
                                console.log(data);
                                Swal.fire(
                                    'Saved!',
                                    'Your product has been saved.',
                                    'success'
                                )
                            }
                        });
                    }
            });

            return false;
        });
    }

    const saveProduct = () => {
        $('#admin-new-product').on('submit', function(e){
            e.preventDefault();

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, update it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire(
                            'Saved!',
                            'Your product has been saved.',
                            'success'
                        )

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            processData: false,
                            contentType: false,
                            dataType: 'JSON',
                        });
                        $.ajax({
                            url: '/product/action/save-product',
                            type: 'POST',
                            data: new FormData($(this)[0]),
                            success: (data) => {
                                //$(this).trigger('reset');
                                console.log(data);
                            }
                        });
                    }
            });
        });
    }

    saveProduct();
    getProduct();
    adminDeleteProduct();
    adminUpdateProduct();
};
