import Swal from 'sweetalert2';

export const AdminHoliday = () => {
    console.log("loading adminAccount JS");

    const onChangeNewHolidayType = () => {
        $('#new-holiday-type').on('change', function(e){
            e.preventDefault();
            var type = $(this).val();

            if(type == 0){
                $('#form-holiday-new-nonrecurrent').removeClass('hidden');
                $('#form-holiday-new-recurrent').addClass('hidden');
            }else{
                $('#form-holiday-new-nonrecurrent').addClass('hidden');
                $('#form-holiday-new-recurrent').removeClass('hidden');
            }
        });
    }

    const getHolidayList = () => {
        /*
        $('#admin-master-account').ready(function () {
            console.log("Taking Data...");
            var table = $('#admin-master-account .yajra-datatable ').DataTable({
                processing: true,
                serverSide: true,
                ajax: "/user/action/get_list_customer",
                columns: [
                  {data: 'id', name: 'id'},
                  {data: 'name', name: 'name'},
                  {data: 'email', name: 'email'},
                  {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
            });
            console.log(table);
        });
        */

        $('#yajra-datatable-list-holidays-recurring').DataTable({
            processing: true,
            serverSide: false,
            ajax: "/holiday-days/action/get_recurring",
            columns: [
              {data: 'name', name: 'name'},
              {data: 'daysOfWeek', name: 'daysOfWeek'},
              {data: 'recurrent_start_time', name: 'recurrent_start_time'},
              {data: 'recurrent_end_time', name: 'recurrent_end_time'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
        });

        $('#yajra-datatable-list-holidays-nonrecurring').DataTable({
            processing: true,
            serverSide: false,
            ajax: "/holiday-days/action/get_nonrecurring",
            columns: [
              {data: 'name', name: 'name'},
              {data: 'start_timestamp', name: 'start_timestamp'},
              {data: 'end_timestamp', name: 'end_timestamp'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
        });

        $('#list-holidays-type').on('change', function(e){
            e.preventDefault();
            var isRecurring = $(this).val();

            console.log(isRecurring);
            if(isRecurring == 0){
                $('#list-holidays-nonrecurring').removeClass('hidden');
                $('#list-holidays-recurring').addClass('hidden');
            }else{
                $('#list-holidays-nonrecurring').addClass('hidden');
                $('#list-holidays-recurring').removeClass('hidden');
            }
        });
    }

    const onSaveHoliday = () => {
        $('#form-holiday-new-nonrecurrent').on('submit', function(e){
            e.preventDefault();

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, save it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire(
                            'Saved!',
                            'Your file has been saved.',
                            'success'
                        )

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            processData: false,
                            contentType: false,
                            dataType: 'JSON',
                        });
                        $.ajax({
                            url: '/holiday-days/action/new-holiday/0',
                            type: 'POST',
                            data: new FormData($(this)[0]),
                            success: (data) => {
                                //$(this).trigger('reset');
                                console.log(data);
                            }
                        });
                    }
            });

            return false;
        });

        $('#form-holiday-new-recurrent').on('submit', function(e){
            e.preventDefault();

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, save it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire(
                            'Saved!',
                            'Your file has been saved.',
                            'success'
                        )

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            processData: false,
                            contentType: false,
                            dataType: 'JSON',
                        });
                        $.ajax({
                            url: '/holiday-days/action/new-holiday/1',
                            type: 'POST',
                            data: new FormData($(this)[0]),
                            success: (data) => {
                                //$(this).trigger('reset');
                                console.log(data);
                            }
                        });
                    }
            });

            return false;
        });
    }

    onSaveHoliday();
    getHolidayList();
    onChangeNewHolidayType();
};
