import Swal from 'sweetalert2';

export const AdminAccount = () => {
    console.log("loading adminAccount JS");

    const getAccount = () => {
        /*
        $('#admin-master-account').ready(function () {
            console.log("Taking Data...");
            var table = $('#admin-master-account .yajra-datatable ').DataTable({
                processing: true,
                serverSide: true,
                ajax: "/user/action/get_list_customer",
                columns: [
                  {data: 'id', name: 'id'},
                  {data: 'name', name: 'name'},
                  {data: 'email', name: 'email'},
                  {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
            });
            console.log(table);
        });
        */

        $('#yajra-datatable-master-account').DataTable({
            processing: true,
            serverSide: false,
            ajax: "/user/action/get_list_customer",
            columns: [
              {data: 'id', name: 'id'},
              {data: 'name', name: 'name'},
              {data: 'email', name: 'email'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
        });
    }

    const adminDeleteAccount = () => {
        $(document).on('submit','#btn-account-delete', function(e){
            e.preventDefault();

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            processData: false,
                            contentType: false,
                            dataType: 'JSON',
                        });
                        $.ajax({
                            url: '/user/action/delete/',
                            type: 'POST',
                            data: new FormData($(this)[0]),
                            success: (data) => {
                                //$(this).trigger('reset');
                                console.log(data);

                                var table = $('#yajra-datatable-master-account').DataTable();
                                table.ajax.reload(null, false);
                            }
                        });
                    }
            });

            return false;
        });
    }

    const adminUpdateAccount = () => {
        $('#admin-account-edit').on('submit', function(e){
            e.preventDefault();
            var accId = String($('#admin-account-edit .accountId').val());
            var url = '/user/action/edit/'+accId;
            console.log(url);
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, update it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire(
                            'Updated!',
                            'Your account has been updated.',
                            'success'
                        )

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            processData: false,
                            contentType: false,
                            dataType: 'JSON',
                        });
                        $.ajax({
                            url: '/user/action/edit/'+accId,
                            type: 'POST',
                            data: new FormData($(this)[0]),
                            success: (data) => {
                                //$(this).trigger('reset');
                                console.log(data);
                            }
                        });
                    }
            });

            return false;
        });
    }

    getAccount();
    adminDeleteAccount();
    adminUpdateAccount();
};
