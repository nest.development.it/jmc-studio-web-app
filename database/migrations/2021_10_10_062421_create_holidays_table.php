<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holidays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description');
            $table->timestamp('start_timestamp',0)->nullable();
            $table->timestamp('end_timestamp',0)->nullable();
            $table->integer('recurrent_day',false)->nullable();
            $table->time('recurrent_start_time',0)->nullable();
            $table->time('recurrent_end_time',0)->nullable();
            $table->tinyInteger('isRecurrent')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('holidays');
    }
}
