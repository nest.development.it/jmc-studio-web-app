<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class HolidaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds= [
            [
                'name' => "Weekend Minggu",
                'description' => "Studio ditutup pada hari minggu",
                'start_timestamp' => null,
                'end_timestamp' => null,
                'recurrent_day' => 0,
                'recurrent_start_time' => Carbon::createFromFormat('H:i:s', '00:00:00'),
                'recurrent_end_time' => Carbon::createFromFormat('H:i:s', '23:59:59'),
                'isRecurrent' => 1,
            ],
            [
                'name' => "Weekend Sabtu",
                'description' => "Studio ditutup pada hari sabtu",
                'start_timestamp' => null,
                'end_timestamp' => null,
                'recurrent_day' => 6,
                'recurrent_start_time' => Carbon::createFromFormat('H:i:s', '00:00:00'),
                'recurrent_end_time' => Carbon::createFromFormat('H:i:s', '23:59:59'),
                'isRecurrent' => 1,
            ],
        ];

        DB::table('holidays')->insert($seeds);
    }
}
