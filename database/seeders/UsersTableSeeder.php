<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds= [
            [
                'name' => 'Ricky',
                'email' => 'ricky1@mhs.stts.edu',
                'password' => '$2a$12$Y4PggLufhIuIOlpcnfVdsuyiKTI2fZ6q2JbX.7AgpVfEno3cgBwQ.',
                'phone_number' => '081235490577',
                'date_of_birth' => '1999-06-15',
                'is_verified' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Jonathan',
                'email' => 'jonathangani279@gmail.com',
                'password' => '$2a$12$zeH.AS4d/gVnbIqeoP1aY.lyPFlgQ.6e.BdtOXMv10LCJNjSRpnWm',
                'phone_number' => '082234700112',
                'date_of_birth' => '1999-01-11',
                'is_verified' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Dicky',
                'email' => 'dickybastian01@gmail.com ',
                'password' => '$2a$12$ESLWDdJbbgi4mjggkemOS.f.co2bomrjSmwIKrm8dw4inNWZ3Bor6',
                'phone_number' => '082244263880',
                'date_of_birth' => '1999-11-11',
                'is_verified' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Ming',
                'email' => 'michaeltenoyo.nest@gmail.com',
                'password' => '$2y$10$IJSqarp00mG1V/jHA33Jwu4oPTLqiF9a/JWawWm8jRz0US9gVhHkS',
                'phone_number' => '6287750362066',
                'date_of_birth' => '1999-11-09',
                'is_verified' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ];

        DB::table('users')->insert($seeds);
    }
}

