<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds= [
            [
                'name' => 'Course',
                'description' => 'Max. 4 kali pertemuan di bulan yang sama',
                'duration' => Carbon::createFromFormat('H:i:s', '00:45:00'),
                'price' => 600000,
                'cut_off' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Live Streaming',
                'description' => 'Bonus sound check 2 jam gratis',
                'duration' => Carbon::createFromFormat('H:i:s', '02:00:00'),
                'price' => 1000000,
                'cut_off' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Solo Recording',
                'description' => '',
                'duration' => Carbon::createFromFormat('H:i:s', '04:00:00'),
                'price' => 1500000,
                'cut_off' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Duet Recording',
                'description' => '2 - 3 orang',
                'duration' => Carbon::createFromFormat('H:i:s', '04:00:00'),
                'price' => 2500000,
                'cut_off' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Band Recording',
                'description' => '5 - 10 orang',
                'duration' => Carbon::createFromFormat('H:i:s', '06:00:00'),
                'price' => 5000000,
                'cut_off' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

        ];

        DB::table('products')->insert($seeds);
    }
}
