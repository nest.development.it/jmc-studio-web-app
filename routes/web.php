<?php

use Illuminate\Support\Facades\Route;

//Auth class routing
use App\Http\Controllers\auth\ViewController as AuthViewController;
use App\Http\Controllers\auth\AuthController;

// Admin class routing
use App\Http\Controllers\admin\ViewController as AdminViewController;
use App\Http\Controllers\admin\UserController as AdminUserController;
use App\Http\Controllers\admin\ProductController as AdminProductController;
use App\Http\Controllers\Admin\TransactionController as AdminTransactionController;
use App\Http\Controllers\Admin\HolidayController as AdminHolidayController;

// CompanyProfile class routing
use App\Http\Controllers\CompanyProfileController;

// Customer class routing
use App\Http\Controllers\customer\ViewController as CustomerViewController;
use App\Http\Controllers\customer\CustomerController;
use App\Http\Controllers\customer\TransactionController;
use Illuminate\Routing\ViewController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function() {
    return view('welcome');
});

//Auth routes
//VIEW
Route::get('/request-reset-password', [AuthViewController::class, 'requestResetPassword']);
Route::get('/reset-password/{token}', [AuthViewController::class, 'resetPassword']);

Route::prefix('/auth')->group(function () {
    //GET
    Route::get('/register-verification/{token}', [AuthController::class, 'registerVerification']);
    Route::get('/logout', [AuthController::class, 'logout']);

    //POST
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::prefix('/reset-password')->group(function () {
        Route::post('/request', [AuthController::class, 'requestResetPassword']);
        Route::post('/reset', [AuthController::class, 'resetPassword']);
    });
});

//Admin routes
Route::group(['domain' => 'admin.localhost'], function () {
    Route::get('/', function () {
        return view('admin.pages.coba');
    });

    // In navigation (VIEW)
    Route::prefix('/holiday-days')->group(function () {
        Route::get('/', [AdminViewController::class, 'holidayDays']);
        Route::get('/set-day', [AdminViewController::class, 'setHolidayDays']);
        Route::get('/action/get_recurring',[AdminViewController::class, 'getRecurringHolidays']);
        Route::get('/action/get_nonrecurring',[AdminViewController::class, 'getNonrecurringHolidays']);
    });
    Route::get('/transaction-approval', [AdminViewController::class, 'transactionApproval']);
    Route::get('/transaction-cashier', [AdminViewController::class, 'transactionCashier']);
    Route::get('/transaction-report',[AdminViewController::class, 'transactionReport']);
    Route::get('/list_customer', [AdminViewController::class, 'list_customer']);
    Route::get('/list_product', [AdminViewController::class, 'list_product']);
    Route::get('/create_product',[AdminViewController::class, 'create_product']);
    Route::get('/get-transaction-approval', [AdminViewController::class, 'getTransactionApproval']);

    // Not in navigation
    Route::prefix('/user')->group(function () {
        Route::get('/{id}',[AdminUserController::class, 'show']);

        // Action
        Route::get('/action/get_list_customer',[AdminViewController::class, 'get_list_customer']);
        Route::post('/action/edit/{id}',[AdminUserController::class, 'edit']);
        Route::post('/action/delete',[AdminUserController::class, 'destroy']);
    });

    Route::prefix('/holiday-days')->group(function () {
        Route::post('/action/new-holiday/{isRecurrent}', [AdminHolidayController::class, 'Create']);
    });

    Route::prefix('/product')->group(function () {
        Route::get('/{id}',[AdminProductController::class, 'show']);

        // Action
        Route::post('/action/save-product',[AdminProductController::class,'save_product']);
        Route::get('/action/get_list_product',[AdminViewController::class,'get_list_product']);
        Route::post('/action/edit/{id}',[AdminProductController::class, 'edit']);
        Route::post('/action/delete',[AdminProductController::class, 'destroy']);
    });

    Route::prefix('/transaction')->group(function () {
        Route::get('/{id}',[AdminProductController::class, 'show']);

        //Action
        Route::post('/action/approve',[AdminTransactionController::class,'verifyPayment']);
        Route::get('/action/get-report',[AdminTransactionController::class,'getReport']);
        Route::post('/action/update-report',[AdminTransactionController::class,'updateReport']);
        Route::post('/action/cashier-input',[AdminTransactionController::class,'cashierInput']);
        Route::get('/action/get-payment-image/{id}',[AdminTransactionController::class,'getPaymentImage']);
    });

    Route::prefix('/calendar')->group(function () {
        //POST
        Route::get('/get-events', [AdminViewController::class, 'getCalendarEvents']);
    });
});

// CompanyProfile routes
//View
Route::get('/', [CompanyProfileController::class, 'landing']);
Route::get('/services', [CompanyProfileController::class, 'services']);
Route::get('/about-us', [CompanyProfileController::class, 'aboutUs']);

//Customer routes
//View
Route::get('/account', [CustomerViewController::class, 'account']);
Route::get('/reservation', [CustomerViewController::class, 'reservation']);

Route::prefix('/customer')->group(function () {
    //POST
    Route::post('/update-account', [CustomerController::class, 'updateAccount']);
    Route::post('/change-password', [CustomerController::class, 'changePassword']);
});

Route::prefix('/transaction')->group(function () {
    //POST
    Route::post('/new-reservation', [TransactionController::class, 'newReservation']);
    Route::get('/get-transaction', [TransactionController::class, 'getTransaction']);
    Route::get('/action/get-detail/{id}',[TransactionController::class,'getDetail']);
    Route::get('/get-history', [CustomerViewController::class, 'getHistory']);
    Route::post('/upload-payment',[TransactionController::class, 'uploadPayment']);
});

Route::prefix('/calendar')->group(function () {
    //POST
    Route::get('/get-events', [CustomerViewController::class, 'getCalendarEvents']);
});
